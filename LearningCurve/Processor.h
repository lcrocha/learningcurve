#pragma once
#include "Types.h"
#include <memory>
#include <functional>

class Processor
{
private:
	std::unique_ptr<INeuralNetworkEngine, decltype(&ReleaseNeuralNetworkEngineInstance)>	_e;
	//Auxilliary
	TransferFunctionType GetRandomTransferFunction();
public:
	Processor();
	~Processor();
	//Initialization
	bool Initialize(IData* d);
	bool Finalize();
	//Operations
	bool Randomize();
	bool ResetStep();
	bool DoLearn(IData* d);
	bool UpdateNeuralNetworkData(IData* d);
	bool UpdateGraphPoints(IData* d);
	bool IncreaseNeuron(IData* d) { return _e->IncreaseNeuron(d->GetLayerCursor() + 1); }
	bool DecreaseNeuron(IData* d) { return _e->DecreaseNeuron(d->GetLayerCursor() + 1); }
	bool AddNeuronLayer(IData* d);
	bool DeleteNeuronLayer(IData* d);
	bool ChangeTransferFunctionType(IData* d);
};
