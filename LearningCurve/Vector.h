#pragma once
#include <cmath>
#include <amp.h>
#include <amp_math.h>

#define MAX(a, b)	(a > b ? a : b)
#define MIN(a, b)	(a < b ? a : b)

typedef float FPOINT;

struct Vector
{
private:
	void ResetIfNan() restrict (cpu)
	{
		if (::isnan(x) || ::isnan(y))
		{
			x = 0.0f;
			y = 0.0f;
		}
	}

	void ResetIfNan() restrict(amp)
	{
	}

public:
	FPOINT x = 0.0f;
	FPOINT y = 0.0f;

	Vector(FPOINT _x = 0.0f, FPOINT _y = 0.0f) restrict (amp, cpu)
	{
		x = _x;
		y = _y;
		ResetIfNan();
	}

	Vector(const Vector& v) restrict (amp, cpu)
	{
		x = v.x;
		y = v.y;
		ResetIfNan();
	}

	inline static Vector Zero()  restrict (amp, cpu)
	{
		return Vector();
	}

	inline static Vector Add(const Vector& v1, const Vector& v2) restrict(amp, cpu)
	{
		return Vector(v1.x + v2.x, v1.y + v2.y);
	}

	inline static Vector Sub(const Vector& v1, const Vector& v2) restrict(amp, cpu)
	{
		return Vector(v1.x - v2.x, v1.y - v2.y);;
	}

	inline static FPOINT Dot(const Vector& v1, const Vector& v2) restrict(amp, cpu)
	{
		return (v1.x * v2.x) + (v1.y * v2.y);
	}

	inline static FPOINT LengthSq(const Vector& v) restrict(amp, cpu)
	{
		return Dot(v, v);
	}

	inline static Vector Scale(const Vector& v, FPOINT n) restrict(amp, cpu)
	{
		return Vector(v.x * n, v.y * n);
	}

	inline static FPOINT Length(const Vector& v) restrict(amp, cpu)
	{
		return (FPOINT)Concurrency::precise_math::sqrt(LengthSq(v));
	}

	inline static Vector Normalize(const Vector& v) restrict(amp, cpu)
	{
		return Scale(v, 1.0f / Length(v));
	}

	inline static Vector Orthogonal(const Vector& v) restrict(amp, cpu)
	{
		return Vector(-v.y, v.x);
	}

	inline static FPOINT LinePointDistance(const Vector& linePoint1, const Vector& linePoint2, const Vector& point) restrict(amp, cpu)
	{
		auto pointVector = Sub(point, linePoint1);
		auto lineVector = Sub(linePoint2, linePoint1);
		auto lengthSq = LengthSq(lineVector);
		auto pointProjectionScale = Dot(pointVector, lineVector);
		pointProjectionScale /= lengthSq;
		auto distanceVector = Scale(lineVector, pointProjectionScale);
		distanceVector = Sub(pointVector, distanceVector);

		return Length(distanceVector);
	}
};

typedef std::vector<Vector>		Vectors;