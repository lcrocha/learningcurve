#include "Engine.h"

Engine::Engine()
{
}

Engine::~Engine()
{
	Finalize();
}

bool Engine::Initialize()
{
	_d.Initialize();
	_v.Initialize();
	_p.Initialize(&_d);

	return false;
}

bool Engine::Finalize()
{
	_d.Finalize();
	_v.Finalize();
	_p.Finalize();

	return false;
}

bool Engine::DoLearn()
{
	auto ret = false;
	if (_p.DoLearn(&_d))
	{
		_d.SetProcessMode(ProcessMode::Learned);
		ret = true;
	}

	return ret;
}

bool Engine::UpdateGraphPoints()
{
	return _p.UpdateGraphPoints(&_d);
}

bool Engine::UpdateNeuralNetworkData()
{
	return _p.UpdateNeuralNetworkData(&_d);
}

bool Engine::DoRender()
{
	return _v.Render(&_d);
}

bool Engine::ResetStep()
{
	return _p.ResetStep();
}

bool Engine::Randomize()
{
	if (_d.GetProcessMode() == ProcessMode::Learned)
	{
		_d.SetProcessMode(ProcessMode::Learning);
	}
	if (_d.GetProcessMode() == ProcessMode::Learning)
	{
		_d.GetDynamicLearningRate().clear();
	}
	_p.ResetStep();

	return _p.Randomize();
}

void Engine::AddDesiredPoint(const Values& input, const Values& output)
{
	if (_d.GetProcessMode() == ProcessMode::Learned)
	{
		_d.SetProcessMode(ProcessMode::Learning);
	}
	if (_d.GetProcessMode() == ProcessMode::Learning)
	{
		_d.GetDynamicLearningRate().clear();
	}
	_d.AddDesiredPoint(input, output);
}

bool Engine::AddNeuronLayer()
{
	if (_d.GetProcessMode() == ProcessMode::Learned)
	{
		_d.SetProcessMode(ProcessMode::Learning);
	}
	if (_d.GetProcessMode() == ProcessMode::Learning)
	{
		_d.GetDynamicLearningRate().clear();
	}

	return _p.AddNeuronLayer(&_d);
}

bool Engine::DeleteNeuronLayer()
{
	if (_d.GetProcessMode() == ProcessMode::Learned)
	{
		_d.SetProcessMode(ProcessMode::Learning);
	}
	if (_d.GetProcessMode() == ProcessMode::Learning)
	{
		_d.GetDynamicLearningRate().clear();
	}

	return _p.DeleteNeuronLayer(&_d);
}

bool Engine::IncreaseNeuron()
{
	if (_d.GetProcessMode() == ProcessMode::Learned)
	{
		_d.SetProcessMode(ProcessMode::Learning);
	}
	if (_d.GetProcessMode() == ProcessMode::Learning)
	{
		_d.GetDynamicLearningRate().clear();
	}

	return _p.IncreaseNeuron(&_d);
}

bool Engine::DecreaseNeuron()
{
	if (_d.GetProcessMode() == ProcessMode::Learned)
	{
		_d.SetProcessMode(ProcessMode::Learning);
	}
	if (_d.GetProcessMode() == ProcessMode::Learning)
	{
		_d.GetDynamicLearningRate().clear();
	}

	return _p.DecreaseNeuron(&_d);
}

bool Engine::ChangeTransferFunctionType()
{
	if (_d.GetProcessMode() == ProcessMode::Learned)
	{
		_d.SetProcessMode(ProcessMode::Learning);
	}
	if (_d.GetProcessMode() == ProcessMode::Learning)
	{
		_d.GetDynamicLearningRate().clear();
	}

	return _p.ChangeTransferFunctionType(&_d);
}

bool Engine::ToggleHelp()
{
	_d.ToggleHelp();

	return _d.IsHelp();
}

void Engine::OnMouseClick(D2D_POINT_2F& p)
{
	Vector pos;
	if (_v.GetCoordinateConverter()->ConvertViewToWorld(p, pos))
	{
		Values input = { pos.x };
		Values output = { pos.y };
		AddDesiredPoint(input, output);
		if (GetProcessMode() != ProcessMode::Learning)
		{
			DoRender();
		}
	}
}

void Engine::OnMouseMove(D2D_POINT_2F& p)
{
	Vector pos;
	if (GetProcessMode() != ProcessMode::Learning && _v.GetCoordinateConverter()->ConvertViewToWorld(p, pos))
	{
		Values input = { pos.x };
		SetLocalInput(input);
		UpdateNeuralNetworkData();
		DoRender();
	}
}

LRESULT Engine::OnCreate(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	return _v.OnCreate(hWnd, wParam, lParam);
}

LRESULT Engine::OnPaint(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	return _v.OnPaint(hWnd, wParam, lParam);
}

LRESULT Engine::OnDestroy(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	return _v.OnDestroy(hWnd, wParam, lParam);
}

LRESULT Engine::OnSize(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	auto ret = _v.OnSize(hWnd, wParam, lParam);
	//Initial data
	auto lt = _v.GetWorldLT();
	auto rb = _v.GetWorldRB();
	SetInputs(lt.x, rb.x, 200);
	UpdateGraphPoints();
	UpdateNeuralNetworkData();
	if (GetProcessMode() != ProcessMode::Learning)
	{
		DoRender();
	}

	return ret;
}

LRESULT Engine::OnDisplayChange(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	return _v.OnDisplayChange(hWnd, wParam, lParam);
}