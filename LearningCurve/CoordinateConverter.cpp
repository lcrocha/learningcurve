#include "CoordinateConverter.h"

CoordinateConverter::CoordinateConverter() : _toViewFactor(0.0f), _toWorldFactor(0.0f)
{
	_viewCenter = { 0, 0 };
	_worldCenter = Vector::Zero();
}

void CoordinateConverter::Update(const D2D_POINT_2F& viewCenter, const Vector& worldCenter, FPOINT maxWorldDistance, const D2D_RECT_F& canvas)
{
	static auto factor = 0.1f;
	_canvas = canvas;
	_viewCenter = viewCenter;
	_worldCenter = Vector::Add(Vector::Scale(_worldCenter, 1.0f - factor), Vector::Scale(worldCenter, factor));
	UpdateMaxDistance(maxWorldDistance);
	//Setting world points
	D2D_POINT_2F lt = { canvas.left + 1, canvas.top + 1 };
	ConvertViewToWorld(lt, _wlt);
	D2D_POINT_2F rb = { canvas.right - 1, canvas.bottom - 1 };
	ConvertViewToWorld(rb, _wrb);
}

void CoordinateConverter::UpdateMaxDistance(FPOINT maxDistance)
{
	_maxDistance = maxDistance;
	//Set converter factor
	_toViewFactor = MIN(_viewCenter.x, _viewCenter.y) / maxDistance;
	_toWorldFactor = 1.0f / _toViewFactor;
}

FPOINT CoordinateConverter::Converter(FPOINT p1, FPOINT c1, FPOINT factor, FPOINT c2, bool invert) const
{
	return c2 + (invert ? -1.0f : 1.0f)*factor*(p1 - c1);
}

FPOINT CoordinateConverter::ConverterToView(FPOINT worldPoint, FPOINT worldCenter, FPOINT viewCenter, bool invert) const
{
	return Converter(worldPoint, worldCenter, _toViewFactor, (FPOINT)viewCenter, invert);
}

FPOINT CoordinateConverter::ConverterToWorld(FPOINT viewPoint, FPOINT viewCenter, FPOINT worldCenter, bool invert) const
{
	return Converter((FPOINT)viewPoint, (FPOINT)viewCenter, _toWorldFactor, worldCenter, invert);
}

FPOINT CoordinateConverter::ConvertLengthToView(FPOINT length) const
{
	return ConverterToView(length, 0.0f, 0, false);
}

bool CoordinateConverter::CheckBoundaries(const D2D_POINT_2F& p1, D2D_POINT_2F& p2) const
{
	auto ret = true;
	p2 = p1;
	if (p1.x < _canvas.left)
	{
		p2.x = _canvas.left;
		ret = false;
	}
	else if (p1.x > _canvas.right)
	{
		p2.x = _canvas.right;
		ret = false;
	}
	if (p1.y < _canvas.top)
	{
		p2.y = _canvas.top;
		ret = false;
	}
	else if (p1.y > _canvas.bottom)
	{
		p2.y = _canvas.bottom;
		ret = false;
	}

	return ret;
}

bool CoordinateConverter::ConvertWorldToView(const Vector & v, D2D1_POINT_2F& p) const
{
	p =
	{
		ConverterToView(v.x, _worldCenter.x, _viewCenter.x, false),
		ConverterToView(v.y, _worldCenter.y, _viewCenter.y, true)
	};

	return CheckBoundaries(p, p);
}

bool CoordinateConverter::ConvertViewToWorld(const D2D1_POINT_2F& p, Vector& v) const
{
	v = Vector(
		ConverterToWorld(p.x, _viewCenter.x, _worldCenter.x, false),
		ConverterToWorld(p.y, _viewCenter.y, _worldCenter.y, true));

	return CheckBoundaries(p, D2D_POINT_2F());
}