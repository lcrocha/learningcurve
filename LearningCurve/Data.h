#pragma once
#include "Types.h"
#include "Vector.h"
#include "NeuralNetworkTypes.h"

class Data : public IData
{
private:
	NeuralNetworkData _nnd;
	Value			_step = 0.0f;
	Count			_epochs = 0;
	Value			_fps = 0.0f;
	Value			_lastError = 0.0f;
	Values			_localInput = Values();
	Values			_linearInputs = Values();
	Vectors			_graphPoints;
	InputsOutputs	_trainset;
	ProcessMode		_processMode = ProcessMode::Stopped;
	Vector			_worldCenter = Vector(0.0f, 0.0f);
	FPOINT			_maxDistance = 1.0f;
	Count			_hiddenLayerCount = 0;
	Count			_layerCursor = 0;
	Value			_desiredError = 1e-5f;
	Counts			_dynamicLearningRate;
	bool			_help = false;
public:
	Data();
	~Data();
	//Initialization
	bool Initialize();
	bool Finalize();
	//Gets and Sets
	const Vector& GetWorldCenter() const { return _worldCenter; }
	void SetWorldCenter(const Vector& wc) { _worldCenter = wc; }
	const ProcessMode& GetProcessMode() const { return _processMode; }
	void SetProcessMode(ProcessMode pm) { _processMode = pm; }
	Value GetMaxDistance() const { return _maxDistance; }
	void SetMaxDistance(Value d) { _maxDistance = d; }
	Value GetLastError() { return _lastError; }
	void SetLastError(Value e) { _lastError = e; }
	const Values& GetLocalInput() const { return _localInput; }
	void SetLocalInput(const Values& input) { _localInput = input; }
	const Values& GetLinearInputs() { return _linearInputs; }
	Value GetFPS() { return _fps; }
	void SetFPS(Value fps) { _fps = fps; }
	Count GetEpochs() { return _epochs; }
	void SetEpochs(Count e) { _epochs = e; }
	Value GetStep() const { return _step; }
	void SetStep(Value s) { _step = s; }
	const InputsOutputs& GetTrainset() const { return _trainset; }
	void GetDesiredPoints(Vectors& desireds) const;
	void DisableLocalInput() { _nnd.localActive = false; }
	const NeuralNetworkData& GetNeuralNetworkData() const { return _nnd; }
	void SetNeuralNetworkData(const NeuralNetworkData& nnd) { _nnd = nnd; }
	Count GetHiddenLayerCount() const { return _hiddenLayerCount; }
	void SetHiddenLayerCount(Count c);
	Count GetLayerCursor() const { return _layerCursor; }
	Value GetDesiredError() const { return _desiredError; }
	void SetDesiredError(Value e) { _desiredError = e; }
	Counts& GetDynamicLearningRate() { return _dynamicLearningRate; }
	void ToggleHelp() { _help = !_help; }
	bool IsHelp() { return _help; }
	//Operations
	bool SetInputs(Value min, Value max, Count steps);
	bool AddDesiredPoint(const Values& input, const Values& output);
	bool DeleteLastDesiredPoint();
	bool DoLayerCursorUp();
	bool DoLayerCursorDown();
	//Access data
	Vectors& GetGraphPoints() { return _graphPoints; }
};

