#pragma once
#include "D2DHelpers.h"
#define _USE_MATH_DEFINES
#include <math.h>

namespace UI
{
	enum class HorizontalOrientation
	{
		Left,
		Center,
		Right
	};

	enum class VerticalOrientation
	{
		Top,
		Center,
		Bottom
	};

	static void lGetTextRect(std::wstring& text, IDWriteFactory* dwFactory, IDWriteTextFormat* textFormat, D2D_POINT_2F& p, D2D_RECT_F& rc, HorizontalOrientation ho, VerticalOrientation vo)
	{
		auto width = 0.0f;
		auto height = 0.0f;
		//Creating text layout
		wrl::ComPtr<IDWriteTextLayout> textLayout;
		dwFactory->CreateTextLayout(text.c_str(), (UINT32)text.size(), textFormat, 0.0f, 0.0f, textLayout.GetAddressOf());
		//Getting min width and size
		textLayout->DetermineMinWidth(&width);
		textLayout->GetFontSize(0, &height);
		//Horizontal
		if (ho == HorizontalOrientation::Left)
		{
			rc.left = p.x;
			rc.right = p.x + width;
		}
		else if (ho == HorizontalOrientation::Center)
		{
			rc.left = p.x - 0.5f*width;
			rc.right = p.x + 0.5f*width;
		}
		else
		{
			rc.left = p.x - width;
			rc.right = p.x;
		}
		//Vertical
		if (vo == VerticalOrientation::Top)
		{
			rc.top = p.y;
			rc.bottom = p.y + height;
		}
		else if (vo == VerticalOrientation::Center)
		{
			rc.top = p.y - 0.5f*height;
			rc.bottom = p.y + 0.5f*height;
		}
		else
		{
			rc.top = p.y - height;
			rc.bottom = p.y;
		}
	}

	struct Brush
	{
		D2D1::ColorF						color = D2D1::ColorF::White;
		wrl::ComPtr<ID2D1SolidColorBrush>	resource;

		Brush(D2D1::ColorF c = D2D1::ColorF::White) : color(c)
		{}

		Brush(const Brush& b) : Brush(b.color)
		{}

		void CreateDeviceResources(ID2D1DeviceContext* dc)
		{
			HR(dc->CreateSolidColorBrush(color, resource.ReleaseAndGetAddressOf()));
		}

		void ReleaseDeviceResources()
		{
			resource.Reset();
		}
	};
	typedef std::vector<Brush>	Brushes;

	struct NeuronVisual
	{
		D2D_POINT_2F p;
	};

	typedef std::vector<NeuronVisual> NeuronVisuals;

	struct LayerVisual
	{
		D2D_RECT_F canvas = {};
		float minRadius = FLT_MAX;
		NeuronVisuals neuronVisuals;
	};

	typedef std::vector<LayerVisual> LayerVisuals;

	struct NeuralNetworkVisual
	{
		float maxRadius = FLT_MIN;
		float minRadius = FLT_MAX;
		float maxOutput = FLT_MIN;
		float minOutput = FLT_MAX;
		float maxBiasWeight = FLT_MIN;
		float minBiasWeight = FLT_MAX;
		float maxWeight = FLT_MIN;
		float minWeight = FLT_MAX;
		float maxWeighted = FLT_MIN;
		float minWeighted = FLT_MAX;

		void Clear()
		{
			maxRadius = FLT_MIN;
			minRadius = FLT_MAX;
			maxOutput = FLT_MIN;
			minOutput = FLT_MAX;
			maxBiasWeight = FLT_MIN;
			minBiasWeight = FLT_MAX;
			maxWeight = FLT_MIN;
			minWeight = FLT_MAX;
			maxWeighted = FLT_MIN;
			minWeighted = FLT_MAX;
		}

		LayerVisuals layerVisuals;
	};

	struct Calculator
	{
	private:
		float _min = FLT_MAX;
		float _max = FLT_MIN;

	public:
		Calculator(float min, float max)
		{
			_min = min;
			_max = max;
		}

		float GetValue(float current)
		{
			float diff = _max - _min;

			return (current - _min) / diff;
		}

		float GetValueNegative(float current)
		{
			return GetValuePositive(::fabsf(current));
			//return current / _min;
		}

		float GetValuePositive(float current)
		{
			auto ret = current;
			if (current < 0.0f)
			{
				ret = 0.0f;
			}
			else if (current > 1.0f)
			{
				ret = 1.0f;
			}

			return ret;
			//return current / _max;
		}
	};

	static D2D1::ColorF GetColor(Calculator& calc, float output)
	{
		D2D1::ColorF color(0.0f, 0.0f, 0.0f);
		if (output >= 0.0f)
		{
			auto value = calc.GetValuePositive(output);
			auto inv = 1.0f - value;
			ASSERT(value <= 1.0f && value >= 0.0f);
			//color = D2D1::ColorF(inv, 1.0f, inv);
			color = D2D1::ColorF(0.0f, 0.7f, 0.5f, value);
			//color = D2D1::ColorF(0.0f, 0.7f, 0.5f, 0.6f * value + 0.4f);
			//color = D2D1::ColorF(0.0f, 0.0f, 0.8f, 0.6f * value + 0.4f);
		}
		else
		{
			auto value = calc.GetValueNegative(output);
			auto inv = 1.0f - value;
			ASSERT(value <= 1.0f && value >= 0.0f);
			//color = D2D1::ColorF(1.0f, inv, inv);
			color = D2D1::ColorF(1.0f, 0.4f, 0.0f, value);
			//color = D2D1::ColorF(1.0f, 0.4f, 0.0f, 0.6f * value + 0.4f);
			//color = D2D1::ColorF(0.8f, 0.0f, 0.0f, 0.6f * value + 0.4f);
		}

		return color;
	}

	static void DrawNeuronGeometry(const D2D1_POINT_2F& center, float radius, float inner, ID2D1Factory* d2dFactory, ID2D1DeviceContext* dc, ID2D1Brush* biasBrush, ID2D1Brush* outputBrush, ID2D1Brush* borderBrush, float strokeWidth)
	{
		wrl::ComPtr<ID2D1PathGeometry> path;
		wrl::ComPtr<ID2D1GeometrySink> sink;
		//Setting points
		D2D_POINT_2F p1 = { center.x + inner, center.y };
		D2D_POINT_2F p2 = { center.x + radius, center.y };
		D2D_POINT_2F p3 = { center.x - inner, center.y };
		D2D_POINT_2F p4 = { center.x - radius, center.y };
		//Creating geometry
		for (auto mode = 0; mode < 2; ++mode)
		{
			ID2D1Brush* brush = nullptr;
			HR(d2dFactory->CreatePathGeometry(path.ReleaseAndGetAddressOf()));
			HR(path->Open(sink.ReleaseAndGetAddressOf()));
			if (mode == 0)
			{
				brush = biasBrush;
				sink->BeginFigure(p1, D2D1_FIGURE_BEGIN_FILLED);
				sink->AddLine(p2);
				sink->AddArc(D2D1::ArcSegment(p4, D2D1::SizeF(radius, radius), 180.0f, D2D1_SWEEP_DIRECTION_COUNTER_CLOCKWISE, D2D1_ARC_SIZE_SMALL));
				sink->AddLine(p3);
				sink->AddArc(D2D1::ArcSegment(p1, D2D1::SizeF(inner, inner), 180.0f, D2D1_SWEEP_DIRECTION_CLOCKWISE, D2D1_ARC_SIZE_SMALL));
				sink->EndFigure(D2D1_FIGURE_END_CLOSED);
				sink->Close();
			}
			else
			{
				brush = outputBrush;
				sink->BeginFigure(p1, D2D1_FIGURE_BEGIN_FILLED);
				sink->AddArc(D2D1::ArcSegment(p3, D2D1::SizeF(inner, inner), 180.0f, D2D1_SWEEP_DIRECTION_COUNTER_CLOCKWISE, D2D1_ARC_SIZE_SMALL));
				sink->AddLine(p4);
				sink->AddArc(D2D1::ArcSegment(p2, D2D1::SizeF(radius, radius), 180.0f, D2D1_SWEEP_DIRECTION_COUNTER_CLOCKWISE, D2D1_ARC_SIZE_SMALL));
				sink->AddLine(p1);
				sink->EndFigure(D2D1_FIGURE_END_CLOSED);
				sink->Close();
			}
			//Drawing geometry
			dc->FillGeometry(path.Get(), brush);
			dc->DrawGeometry(path.Get(), borderBrush, 0.5f*strokeWidth);
		}
	}

	static void DrawFilledArc(const D2D1_POINT_2F& center, float radius, ID2D1Factory* d2dFactory, ID2D1DeviceContext* dc, ID2D1Brush* brush, bool clockwise)
	{
		wrl::ComPtr<ID2D1PathGeometry> path;
		HR(d2dFactory->CreatePathGeometry(path.GetAddressOf()));
		wrl::ComPtr<ID2D1GeometrySink> sink;
		HR(path->Open(sink.GetAddressOf()));
		//Setting points
		D2D_POINT_2F p1 = { center.x, center.y + radius };
		D2D_POINT_2F p2 = { center.x, center.y - radius };
		//Begin geometry
		sink->BeginFigure(center, D2D1_FIGURE_BEGIN_FILLED);
		sink->AddLine(p1);
		sink->AddArc(D2D1::ArcSegment(
			p2,
			D2D1::SizeF(radius, radius),
			0.0f, clockwise ? D2D1_SWEEP_DIRECTION_CLOCKWISE : D2D1_SWEEP_DIRECTION_COUNTER_CLOCKWISE,
			D2D1_ARC_SIZE_SMALL));
		sink->EndFigure(D2D1_FIGURE_END_CLOSED);
		sink->Close();
		//Drawing geometry
		dc->FillGeometry(path.Get(), brush);
	}

	static const wchar_t* lGetProcessMode(const ProcessMode& pm)
	{
		switch (pm)
		{
		case ProcessMode::Learned:
			return L"Learned";
		case ProcessMode::Learning:
			return L"Learning";
		}

		return L"Stopped";
	}

	static float GetColorAnimationFactor()
	{
		auto ang = 2.0f * (float)M_PI * (::GetTickCount64() % 1000) / 1000.0f;
		return 0.5f * ::sinf(ang) + 0.5f;
	}

	static void CreateTextFormat(IDWriteFactory* dw, wrl::ComPtr<IDWriteTextFormat>& textFormat, float size, DWRITE_FONT_STYLE fontStyle, DWRITE_FONT_WEIGHT fontWeight)
	{
		HR(dw->CreateTextFormat(L"Tahoma", NULL, fontWeight, fontStyle, DWRITE_FONT_STRETCH_NORMAL, size, L"", textFormat.GetAddressOf()));
		textFormat->SetWordWrapping(DWRITE_WORD_WRAPPING_NO_WRAP);
		textFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
		textFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
	}
};