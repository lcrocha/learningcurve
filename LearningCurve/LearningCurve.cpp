// LearningCurve.cpp : Defines the entry point for the application.
//

#include "LearningCurve.h"
#include "Engine.h"
#include "ChronoTimer.h"
#include "Resource.h"
#include "Memory.h"
#include <Windows.h>
#include <windowsx.h>
#include <commctrl.h>

#pragma comment(lib, "MinCore")
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"") 


#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int, LPVOID);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    AboutProc(HWND, UINT, WPARAM, LPARAM);

bool _visible = true;
SIZE _minWindowSize = { 600, 400 };

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);
	MemoryWatcher m;

    // TODO: Place code here.
	Engine e;

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_LEARNINGCURVE, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance(hInstance, nCmdShow, &e))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_LEARNINGCURVE));

	MSG msg = {};
	// Main message loop:
	while (msg.message != WM_QUIT)
	{
		if (e.GetProcessMode() == ProcessMode::Learning && _visible)
		{
			auto learned = false;
			auto i = 0;
			ChronoTimer t;
			//Process
			do
			{
				++i;
			} while (!e.DoLearn() && t.GetPartialTime() < 20);
			t.Stop();
			//e.ResetStep();
			e.UpdateGraphPoints();
			e.UpdateNeuralNetworkData();
			e.DoRender();
			//Getting fps
			static ChronoTimer timer;
			static ChronoTimer fps;
			if (timer.GetPartialTime() > 500)
			{
				timer.Stop().Start();
				auto time = (Value)fps.Stop().GetTime();
				e.SetFPS(1000.0f / time);
				auto elapsed = t.GetTime();
				if (elapsed > 0)
				{
					e.SetEpoch(1000 * i / elapsed);
				}
				else
				{
					e.SetEpoch(0);
				}
			}
			fps.Start();

			//Process all pending messages
			while (::PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE) && msg.message != WM_QUIT)
			{
				if (!::TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
				{
					::TranslateMessage(&msg);
					::DispatchMessage(&msg);
				}
			}
		}
		else
		{
			if (::GetMessage(&msg, nullptr, 0, 0) && msg.message != WM_QUIT)
			{
				if (!::TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
				{
					::TranslateMessage(&msg);
					::DispatchMessage(&msg);
				}
			}
		}
	}

    return (int) msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_LEARNINGCURVE));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_LEARNINGCURVE);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow, LPVOID userData)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, _minWindowSize.cx, _minWindowSize.cy, nullptr, nullptr, hInstance, userData);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static Engine* e = nullptr;
    switch (message)
    {
    case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, AboutProc);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_CREATE:
	{
		auto c = reinterpret_cast<CREATESTRUCT*>(lParam);
		e = (Engine*)c->lpCreateParams;
		e->Initialize();
		e->OnCreate(hWnd, wParam, lParam);
		e->SetDesiredError(1e-4f);
		e->SetProcessMode(ProcessMode::Learning);
		e->AddDesiredPoint(Values() = { 0.0f }, Values() = { 0.0f });
		/*{
			auto s = (unsigned)21;
			for (unsigned i = 0L; i < s; ++i)
			{
				auto x = (float)(-1.0f + 2.0f * (float)i / (float)(s - 1));
				auto y = (float)0.5f*::sinf(2.0f*M_PI*x);
				e->AddDesiredPoint(Values() = { x }, Values() = { y });
			}
		}*/

	}
	break;
	case WM_KEYDOWN:
	{
		auto update = false;
		auto updateGraphPoints = false;
		switch (wParam)
		{
		case VK_SPACE:
		{
			if (e->GetProcessMode() == ProcessMode::Stopped)
			{
				e->SetProcessMode(ProcessMode::Learning);
			}
			else
			{
				e->SetProcessMode(ProcessMode::Stopped);
				update = true;
			}
		}
		break;
		case VK_LEFT:
			updateGraphPoints = e->DecreaseNeuron();
			break;
		case VK_RIGHT:
			updateGraphPoints = e->IncreaseNeuron();
			break;
		case VK_UP:
			update = e->DoLayerCursorUp();
			break;
		case VK_DOWN:
			update = e->DoLayerCursorDown();
			break;
		case 'A':
		case 'a':
			updateGraphPoints = e->AddNeuronLayer();
			break;
		case 'D':
		case 'd':
			updateGraphPoints = e->DeleteNeuronLayer();
			break;
		case VK_DELETE:
			update = e->DeleteLastDesiredPoint();
			break;
		case 'r':
		case 'R':
			updateGraphPoints = e->Randomize();
			break;
		case 't':
		case 'T':
			updateGraphPoints = e->ChangeTransferFunctionType();
			break;
		case 'h':
		case 'H':
			e->ToggleHelp();
			update = true;
			break;
		};
		if (updateGraphPoints)
		{
			update = e->UpdateGraphPoints();
			e->UpdateNeuralNetworkData();
		}
		if (update && e->GetProcessMode() != ProcessMode::Learning)
		{
			e->DoRender();
		}
	}
	break;
	case WM_ACTIVATE:
	{
		_visible = !HIWORD(wParam);
		break;
	}
    case WM_PAINT:
    {
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(hWnd, &ps);
        EndPaint(hWnd, &ps);
		e->OnPaint(hWnd, wParam, lParam);
    }
    break;
	case WM_SIZE:
	{
		e->OnSize(hWnd, wParam, lParam);
	}
	break;
    case WM_DESTROY:
	{
		e->OnDestroy(hWnd, wParam, lParam);
		e->Finalize();
		PostQuitMessage(0);
	}
	break;
	case WM_LBUTTONDOWN:
	{
		D2D_POINT_2F p = { (float)GET_X_LPARAM(lParam), (float)GET_Y_LPARAM(lParam) };
		e->OnMouseClick(p);
	}
	break;
	case WM_MOUSEMOVE:
	{
		D2D_POINT_2F p = { (float)GET_X_LPARAM(lParam), (float)GET_Y_LPARAM(lParam) };
		e->OnMouseMove(p);
	}
	break;
	case WM_GETMINMAXINFO:
	{
		auto info = reinterpret_cast<MINMAXINFO*>(lParam);
		info->ptMinTrackSize.x = _minWindowSize.cx;
		info->ptMinTrackSize.y = _minWindowSize.cy;
		break;
	}
	case WM_DISPLAYCHANGE:
	{
		e->OnDisplayChange(hWnd, wParam, lParam);
	}
	break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

static std::wstring lGetVersion(WCHAR* entry)
{
	std::wstring ret;

	HRSRC hVersion = ::FindResource(hInst, MAKEINTRESOURCE(VS_VERSION_INFO), RT_VERSION);
	if (hVersion != NULL)
	{
		HGLOBAL hGlobal = ::LoadResource(hInst, hVersion);
		if (hGlobal != NULL)
		{
			LPVOID versionInfo = ::LockResource(hGlobal);
			if (versionInfo != NULL)
			{
				DWORD vLen, langD;
				BOOL retVal;
				WCHAR* retbuf = NULL;
				static WCHAR fileEntry[256] = {};
				wsprintf(fileEntry, L"\\VarFileInfo\\Translation");
				retVal = ::VerQueryValue(versionInfo, fileEntry, (LPVOID*)&retbuf, (UINT *)&vLen);
				if (retVal && vLen == 4)
				{
					memcpy(&langD, retbuf, 4);
					wsprintf(fileEntry, L"\\StringFileInfo\\%02X%02X%02X%02X\\%s",
						(langD & 0xff00) >> 8, langD & 0xff, (langD & 0xff000000) >> 24,
						(langD & 0xff0000) >> 16, entry);
				}
				else
					wsprintf(fileEntry, L"\\StringFileInfo\\%04X04B0\\%s",
						::GetUserDefaultLangID(), entry);

				if (::VerQueryValue(versionInfo, fileEntry, (LPVOID*)&retbuf, (UINT *)&vLen))
					ret = (WCHAR*)retbuf;
			}
		}
		::FreeResource(hGlobal);
	}

	return ret;
}

// Message handler for about box.
INT_PTR CALLBACK AboutProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
	{
		auto version = lGetVersion(L"FileVersion");
		std::wstringstream wss;
		wss << L"Neural Network " << version << L"\nAuthor: Leonardo Correa Rocha\nleonardo.correa.rocha@hotmail.com";
		::SetDlgItemTextW(hDlg, IDC_ABOUT_TEXT, wss.str().c_str());

		return (INT_PTR)TRUE;
	}

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
	case WM_NOTIFY:
	{
		LPNMHDR pnmh = (LPNMHDR)lParam;
		// If the notification came from the syslink control
		if (pnmh->idFrom == IDC_SYSLINK)
		{
			// NM_CLICK is the notification is normally used.
			// NM_RETURN is the notification needed for return keypress, otherwise the control is not keyboard accessible.
			if ((pnmh->code == NM_CLICK) || (pnmh->code == NM_RETURN))
			{
				// Recast lParam to an NMLINK item because it also contains NMHDR as part of its structure
				PNMLINK link = (PNMLINK)lParam;
				::ShellExecute(NULL, L"open", link->item.szUrl, NULL, NULL, SW_SHOWNORMAL);
			}
		}
		break;
	}
	}
    return (INT_PTR)FALSE;
}
