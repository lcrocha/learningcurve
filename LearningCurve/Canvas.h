#pragma once

namespace UI
{
	class Canvas : public IElement
	{
	public:
		virtual void CreateDeviceIndependentResources(ID2D1Factory* d2d, IDWriteFactory* dw)
		{}

		virtual void CreateDeviceResources(ID2D1DeviceContext* dc)
		{}

		virtual void CreateDeviceSizeResources(ID2D1DeviceContext* dc)
		{}

		virtual void ReleaseDeviceResources()
		{}

		virtual void Render(ID2D1Factory* d2d, IDWriteFactory* dw, ID2D1DeviceContext* dc, ICoordinateConverter* cc, IData* d, Brushes& brushes)
		{
			dc->Clear(D2D1::ColorF(D2D1::ColorF::Black));
		}
	};
}