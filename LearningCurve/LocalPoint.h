#pragma once

namespace UI
{
	class LocalPoint : public IElement
	{
	private:
		wrl::ComPtr<ID2D1SolidColorBrush>	_brush;
		wrl::ComPtr<ID2D1StrokeStyle>		_stroke;

	public:
		virtual void CreateDeviceIndependentResources(ID2D1Factory* d2d, IDWriteFactory* dw)
		{
			//Creating stroke
			D2D1_STROKE_STYLE_PROPERTIES props = {};
			props.lineJoin = D2D1_LINE_JOIN_ROUND;
			props.dashStyle = D2D1_DASH_STYLE_DASH;
			props.dashCap = D2D1_CAP_STYLE_ROUND;
			HR(d2d->CreateStrokeStyle(props, nullptr, 0, _stroke.GetAddressOf()));
		}

		virtual void CreateDeviceResources(ID2D1DeviceContext* dc)
		{
			HR(dc->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Red), _brush.ReleaseAndGetAddressOf()));
		}

		virtual void CreateDeviceSizeResources(ID2D1DeviceContext* dc)
		{}

		virtual void ReleaseDeviceResources()
		{
			_brush.Reset();
		}

		virtual void Render(ID2D1Factory* d2d, IDWriteFactory* dw, ID2D1DeviceContext* dc, ICoordinateConverter* cc, IData* d, Brushes& brushes)
		{
			const auto& nnd = d->GetNeuralNetworkData();
			//D2D bbjects
			wrl::ComPtr<IDWriteTextFormat> textFormat;
			//Creating text format
			HR(dw->CreateTextFormat(L"Tahoma", NULL, DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 0.008f*_renderCanvasSize.width, L"", textFormat.GetAddressOf()));
			textFormat->SetWordWrapping(DWRITE_WORD_WRAPPING_NO_WRAP);
			textFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
			textFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
			//Drawing local point
			if (nnd.localActive)
			{
				//Local point data
				auto& input = nnd.local.input;
				auto& output = nnd.local.output;
				Vector local(input[0], output[0]);
				D2D_POINT_2F pLocal;
				if (cc->ConvertWorldToView(local, pLocal))
				{
					Vector toX(0.0f, local.y), toY(local.x, 0.0f);
					D2D_POINT_2F pToX, pToY;
					D2D_RECT_F rc = {};
					//Drawing auxilliary axis
					_brush->SetColor(D2D1::ColorF(D2D1::ColorF::LightGray));
					if (cc->ConvertWorldToView(toX, pToX))
					{
						std::wstringstream oss;
						oss << std::setprecision(2) << std::fixed << local.y;
						std::wstring text = oss.str();
						lGetTextRect(text, dw, textFormat.Get(), pToX, rc, local.x > 0.0f ? HorizontalOrientation::Right : HorizontalOrientation::Left, VerticalOrientation::Center);
						dc->DrawTextW(text.c_str(), (UINT32)text.size(), textFormat.Get(), rc, _brush.Get());
						dc->DrawLine(pLocal, pToX, _brush.Get(), 1.0f, _stroke.Get());
					}
					if (cc->ConvertWorldToView(toY, pToY))
					{
						std::wstringstream oss;
						oss << std::setprecision(2) << std::fixed << local.x;
						std::wstring text = oss.str();
						lGetTextRect(text, dw, textFormat.Get(), pToY, rc, HorizontalOrientation::Center, local.y > 0.0f ? VerticalOrientation::Top : VerticalOrientation::Bottom);
						dc->DrawTextW(text.c_str(), (UINT32)text.size(), textFormat.Get(), rc, _brush.Get());
						dc->DrawLine(pLocal, pToY, _brush.Get(), 1.0f, _stroke.Get());
					}
					//Drawing local point
					_brush->SetColor(D2D1::ColorF(D2D1::ColorF::Red));
					dc->FillEllipse(D2D1::Ellipse(pLocal, 3, 3), _brush.Get());
				}
			}
		}
	};
}