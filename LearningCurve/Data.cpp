#include "Data.h"

Data::Data()
{
}

Data::~Data()
{
}

bool Data::Initialize()
{
	return false;
}

bool Data::Finalize()
{
	return false;
}

bool Data::SetInputs(Value min, Value max, Count steps)
{
	auto diff = max - min;
	//Adjusting vectors
	_linearInputs.resize(steps);
	_graphPoints.resize(steps);
	//Filling inputs
	for (Count i = 0; i < steps; ++i)
	{
		auto v = min + (Value)i*diff / (Value)(steps - 1);
		_linearInputs[i] = v;
	}

	return false;
}

void Data::GetDesiredPoints(Vectors& desireds) const
{
	desireds.resize(_trainset.size());
	std::transform(_trainset.begin(), _trainset.end(), desireds.begin(), [](const InputOutput& io)
	{
		auto& i = io.input;
		auto& o = io.output;

		return Vector(i[0], o[0]);
	});
}

bool Data::AddDesiredPoint(const Values& input, const Values& output)
{
	_trainset.push_back(InputOutput(input, output));

	return true;
}

bool Data::DeleteLastDesiredPoint()
{
	auto ret = false;
	if (_trainset.size() > 0)
	{
		_trainset.resize(_trainset.size() - 1);
		ret = true;
	}

	return ret;
}

bool Data::DoLayerCursorUp()
{
	auto ret = false;
	if (_layerCursor > 0)
	{
		_layerCursor--;
		ret = true;
	}

	return ret;
}

bool Data::DoLayerCursorDown()
{
	auto ret = false;
	if (_layerCursor < _hiddenLayerCount - 1)
	{
		_layerCursor++;
		ret = true;
	}

	return ret;
}

void Data::SetHiddenLayerCount(Count c)
{
	_hiddenLayerCount = c;
	if (_layerCursor >= _hiddenLayerCount)
	{
		_layerCursor = _hiddenLayerCount - 1;
	}
}