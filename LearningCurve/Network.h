#pragma once
#include "ChronoTimer.h"
#include <iomanip>

namespace UI
{
	class Network : public IElement
	{
	private:
		NeuralNetworkData					_oldnnd;
		ChronoTimer							_outputOpacityTimer;
		wrl::ComPtr<ID2D1SolidColorBrush>	_backBrush;
		wrl::ComPtr<ID2D1SolidColorBrush>	_borderBrush;
		wrl::ComPtr<ID2D1SolidColorBrush>	_cursorBrush;
		wrl::ComPtr<ID2D1SolidColorBrush>	_biasBrush;
		wrl::ComPtr<ID2D1SolidColorBrush>	_outputBrush;
		wrl::ComPtr<ID2D1SolidColorBrush>	_weightsBrush;
		wrl::ComPtr<ID2D1SolidColorBrush>	_statusBrush;
		wrl::ComPtr<ID2D1StrokeStyle>		_dashStroke;

	public:
		virtual void CreateDeviceIndependentResources(ID2D1Factory* d2d, IDWriteFactory* dw)
		{
			//Creating stroke
			D2D1_STROKE_STYLE_PROPERTIES props = {};
			props.lineJoin = D2D1_LINE_JOIN_ROUND;
			props.dashStyle = D2D1_DASH_STYLE_DASH;
			props.dashCap = D2D1_CAP_STYLE_FLAT;
			HR(d2d->CreateStrokeStyle(props, nullptr, 5, _dashStroke.GetAddressOf()));
		}

		virtual void CreateDeviceResources(ID2D1DeviceContext* dc)
		{
			HR(dc->CreateSolidColorBrush(D2D1::ColorF(0.1f, 0.1f, 0.1f), _backBrush.ReleaseAndGetAddressOf()));
			HR(dc->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::DarkGray), _borderBrush.ReleaseAndGetAddressOf()));
			HR(dc->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::SandyBrown), _cursorBrush.ReleaseAndGetAddressOf()));
			HR(dc->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::White), _biasBrush.ReleaseAndGetAddressOf()));
			HR(dc->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::White), _outputBrush.ReleaseAndGetAddressOf()));
			HR(dc->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::White), _weightsBrush.ReleaseAndGetAddressOf()));
			HR(dc->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::WhiteSmoke), _statusBrush.ReleaseAndGetAddressOf()));
		}

		virtual void CreateDeviceSizeResources(ID2D1DeviceContext* dc)
		{}

		virtual void ReleaseDeviceResources()
		{
			_backBrush.Reset();
			_borderBrush.Reset();
			_cursorBrush.Reset();
			_biasBrush.Reset();
			_outputBrush.Reset();
			_weightsBrush.Reset();
			_statusBrush.Reset();
		}

		virtual void Render(ID2D1Factory* d2d, IDWriteFactory* dw, ID2D1DeviceContext* dc, ICoordinateConverter* cc, IData* d, Brushes& brushes)
		{
			NeuralNetworkVisual neuralNetworkVisual;
			auto& nnd = d->GetNeuralNetworkData();
			auto pm = d->GetProcessMode();
			//Clearing area
			dc->FillRectangle(_renderCanvas, _backBrush.Get());
			//Setting border
			dc->DrawRectangle(_renderCanvas, _borderBrush.Get());
			//Status canvas size
			D2D_SIZE_F statusCanvasSize = { _renderCanvasSize.width, 0.1f * _renderCanvasSize.height };
			//Layer text canvas
			D2D_SIZE_F layerTextCanvasSize = { 0.05f * _renderCanvasSize.width, _renderCanvasSize.height - statusCanvasSize.height };
			//Engine canvas
			auto borderStrokeWidth = (0.01f * _renderCanvasSize.width);
			auto displacement = (1.5f * borderStrokeWidth);
			D2D_SIZE_F engineCanvasSize = { _renderCanvasSize.width - layerTextCanvasSize.width - 2 * displacement, layerTextCanvasSize.height - 2 * displacement };
			D2D_POINT_2F engineCanvasAnchor = { _renderCanvas.left + layerTextCanvasSize.width + displacement, _renderCanvas.top + displacement };
			D2D_RECT_F engineCanvas = { engineCanvasAnchor.x, engineCanvasAnchor.y , engineCanvasAnchor.x + engineCanvasSize.width, engineCanvasAnchor.y + engineCanvasSize.height };
			//Layers
			auto totalLayers = (int)nnd.neuronLayerDatas.size();
			neuralNetworkVisual.layerVisuals.resize(totalLayers);
			D2D_SIZE_F layerCanvasSize = { engineCanvasSize.width, engineCanvasSize.height / totalLayers };
			for (auto l = 0; l < totalLayers; ++l)
			{
				auto& layerData = nnd.neuronLayerDatas[l];
				auto& layerVisual = neuralNetworkVisual.layerVisuals[l];
				D2D_POINT_2F layerAnchor = { engineCanvas.left, engineCanvas.top + l * layerCanvasSize.height };
				layerVisual.canvas = { layerAnchor.x, layerAnchor.y, layerAnchor.x + layerCanvasSize.width, layerAnchor.y + layerCanvasSize.height };
				//Neurons
				auto totalNeurons = (int)layerData.neuronDatas.size();
				layerVisual.neuronVisuals.resize(totalNeurons);
				D2D_SIZE_F neuronSize = { layerCanvasSize.width / totalNeurons, layerCanvasSize.height };
				neuralNetworkVisual.maxRadius = MAX(neuralNetworkVisual.maxRadius, neuronSize.width);
				neuralNetworkVisual.maxRadius = MAX(neuralNetworkVisual.maxRadius, neuronSize.height);
				neuralNetworkVisual.minRadius = MIN(neuralNetworkVisual.minRadius, neuronSize.width);
				neuralNetworkVisual.minRadius = MIN(neuralNetworkVisual.minRadius, neuronSize.height);
				layerVisual.minRadius = MIN(layerVisual.minRadius, neuronSize.width);
				layerVisual.minRadius = MIN(layerVisual.minRadius, neuronSize.height);
				for (auto n = 0; n < totalNeurons; ++n)
				{
					auto& neuronData = layerData.neuronDatas[n];
					auto& neuronVisual = layerVisual.neuronVisuals[n];
					D2D_POINT_2F neuronAnchor = { layerVisual.canvas.left + n * neuronSize.width , layerVisual.canvas.top };
					D2D_RECT_F neuronCanvas = { neuronAnchor.x, neuronAnchor.y, neuronAnchor.x + neuronSize.width, neuronAnchor.y + neuronSize.height };
					neuronVisual.p = { (neuronCanvas.left + neuronCanvas.right) / 2.0f, (neuronCanvas.top + neuronCanvas.bottom) / 2.0f };
					//Output
					neuralNetworkVisual.minOutput = MIN(neuralNetworkVisual.minOutput, neuronData.output);
					neuralNetworkVisual.maxOutput = MAX(neuralNetworkVisual.maxOutput, neuronData.output);
					//Bias
					neuralNetworkVisual.minBiasWeight = MIN(neuralNetworkVisual.minBiasWeight, neuronData.biasWeight);
					neuralNetworkVisual.maxBiasWeight = MAX(neuralNetworkVisual.maxBiasWeight, neuronData.biasWeight);
					//Output weights
					for (auto& w : neuronData.outputWeights)
					{
						neuralNetworkVisual.minWeight = MIN(neuralNetworkVisual.minWeight, w);
						neuralNetworkVisual.maxWeight = MAX(neuralNetworkVisual.maxWeight, w);
					}
					//weighted Outputs
					for (auto& wed : neuronData.weightedOutputs)
					{
						neuralNetworkVisual.minWeighted = MIN(neuralNetworkVisual.minWeighted, wed);
						neuralNetworkVisual.maxWeighted = MAX(neuralNetworkVisual.maxWeighted, wed);
					}
				}
			}
			//D2D Objects
			wrl::ComPtr<IDWriteTextFormat> layerTextFormat;
			CreateTextFormat(dw, layerTextFormat, MIN(0.9f * layerTextCanvasSize.width, 0.9f * layerCanvasSize.height), DWRITE_FONT_STYLE_ITALIC, DWRITE_FONT_WEIGHT_NORMAL);
			wrl::ComPtr<IDWriteTextFormat> layerTextCursorFormat;
			CreateTextFormat(dw, layerTextCursorFormat, MIN(0.9f * layerTextCanvasSize.width, 0.9f * layerCanvasSize.height), DWRITE_FONT_STYLE_ITALIC, DWRITE_FONT_WEIGHT_BOLD);
			wrl::ComPtr<IDWriteTextFormat> layerInsideTextFormat;
			CreateTextFormat(dw, layerInsideTextFormat, 0.1f * layerCanvasSize.height, DWRITE_FONT_STYLE_ITALIC, DWRITE_FONT_WEIGHT_NORMAL);
			wrl::ComPtr<IDWriteTextFormat> layerInsideTextCursorFormat;
			CreateTextFormat(dw, layerInsideTextCursorFormat, 0.1f * layerCanvasSize.height, DWRITE_FONT_STYLE_ITALIC, DWRITE_FONT_WEIGHT_BOLD);
			auto helpBulletRadius = 1.0f * borderStrokeWidth;
			auto helpStrokeWidth = 0.5f * helpBulletRadius;
			//Setting for animation
			if (pm == ProcessMode::Learned || pm == ProcessMode::Stopped)
			{
				_outputOpacityTimer.Start();
				_outputBrush->SetOpacity(1.0f);
			}
			//Drawing objects
			Calculator outputCalc(neuralNetworkVisual.minOutput, neuralNetworkVisual.maxOutput);
			Calculator biasCalc(neuralNetworkVisual.minBiasWeight, neuralNetworkVisual.maxBiasWeight);
			Calculator weightCalc(neuralNetworkVisual.minWeight, neuralNetworkVisual.maxWeight);
			Calculator weightedCalc(neuralNetworkVisual.minWeighted, neuralNetworkVisual.maxWeighted);
			for (auto l = 0; l < totalLayers; ++l)
			{
				auto cursor = (d->GetLayerCursor() + 1 == l);
				auto& layerVisual = neuralNetworkVisual.layerVisuals[l];
				auto& layerData = nnd.neuronLayerDatas[l];
				ASSERT(layerVisual.neuronVisuals.size() == layerData.neuronDatas.size());
				auto totalNeurons = (int)layerData.neuronDatas.size();
				//Drawing lines
				bool firstWeightHelp = false;
				D2D_POINT_2F firstWeightPoint = {};
				for (auto n = 0; n < totalNeurons; ++n)
				{
					auto& neuronVisual = layerVisual.neuronVisuals[n];
					auto& neuronData = layerData.neuronDatas[n];
					if (l < totalLayers - 1)
					{
						auto& nextLayerVisual = neuralNetworkVisual.layerVisuals[l + 1];
						for (auto nn = 0; nn < (int)nextLayerVisual.neuronVisuals.size(); ++nn)
						{
							auto& nextNeuronVisual = nextLayerVisual.neuronVisuals[nn];
							//Getting neuron data
							auto weighted = neuronData.weightedOutputs[nn];
							auto weight = neuronData.outputWeights[nn];
							if (pm == ProcessMode::Learning)
							{
								_weightsBrush->SetColor(GetColor(weightCalc, weight));
							}
							else
							{
								_weightsBrush->SetColor(GetColor(weightedCalc, weighted));
							}
							auto strokeWidth = 0.2f + 0.1f * neuralNetworkVisual.minRadius * (weight >= 0.0f ? weightCalc.GetValuePositive(weight) : weightCalc.GetValueNegative(weight));
							ASSERT(strokeWidth > 0.0f);
							//Testing old data
							if (pm == ProcessMode::Learning && l < (int)_oldnnd.neuronLayerDatas.size() && n < (int)_oldnnd.neuronLayerDatas[l].neuronDatas.size() && nn < (int)_oldnnd.neuronLayerDatas[l].neuronDatas[n].outputWeights.size())
							{
								auto& oldWeight = _oldnnd.neuronLayerDatas[l].neuronDatas[n].outputWeights[nn];
								if (oldWeight != weight)
								{
									_weightsBrush->SetOpacity(0.1f);
								}
								else
								{
									_weightsBrush->SetOpacity(0.4f);
								}
							}
							else
							{
								_weightsBrush->SetOpacity(0.4f);
							}
							dc->DrawLine(neuronVisual.p, nextNeuronVisual.p, _weightsBrush.Get(), strokeWidth);
							//Drawing weight helper
							if (d->IsHelp() && (pm == ProcessMode::Learned || pm == ProcessMode::Stopped) && (l == totalLayers - 2))
							{
								Vector v1 = { nextNeuronVisual.p.x - neuronVisual.p.x, nextNeuronVisual.p.y - neuronVisual.p.y };
								Vector v1N = Vector::Normalize(v1);
								auto v1L = Vector::Length(v1);
								Vector v2 = Vector::Scale(v1N, 0.4f*v1L);
								D2D_POINT_2F p2 = { neuronVisual.p.x + v2.x, neuronVisual.p.y + v2.y };
								dc->FillEllipse(D2D1::Ellipse(p2, helpBulletRadius, helpBulletRadius), brushes[0].resource.Get());
								if (!firstWeightHelp)
								{
									firstWeightPoint = p2;
									firstWeightHelp = true;
								}
								
							}
						}
					}
					//Drawing layer canvas border
					if (l == 0 || l >= totalLayers - 2)
					{
						auto rrBrush = _borderBrush.Get();
						D2D1_ROUNDED_RECT rr = {};
						rr.radiusX = (float)borderStrokeWidth;
						rr.radiusY = (float)borderStrokeWidth;
						if (l == totalLayers - 2)
						{
							auto& firstHiddenVisual = neuralNetworkVisual.layerVisuals[1];
							auto& lastHiddenVisual = neuralNetworkVisual.layerVisuals[totalLayers - 2];
							//Hidden layers
							rr.rect = { firstHiddenVisual.canvas.left, firstHiddenVisual.canvas.top, lastHiddenVisual.canvas.right, lastHiddenVisual.canvas.bottom };
						}
						else
						{
							//Input or output layer
							rr.rect = { layerVisual.canvas.left, layerVisual.canvas.top, layerVisual.canvas.right, layerVisual.canvas.bottom};
							if (l == 0)
							{
								rr.rect.bottom -= (float)displacement;
							}
							else
							{
								rr.rect.top += (float)displacement;
							}
						}
						dc->DrawRoundedRectangle(rr, rrBrush, (float)borderStrokeWidth);
					}
					else
					{
						//Separators
						dc->DrawLine(
							D2D1_POINT_2F() = { layerVisual.canvas.left, layerVisual.canvas.bottom },
							D2D1_POINT_2F() = { layerVisual.canvas.right, layerVisual.canvas.bottom },
							_borderBrush.Get(), 0.5f*borderStrokeWidth, _dashStroke.Get());
					}
					//Drawing transfer function text
					if(l > 0 && l < totalLayers - 1)
					{
						D2D_POINT_2F anchor = { layerVisual.canvas.left + 2.0f * borderStrokeWidth, layerVisual.canvas.top + 2.0f * borderStrokeWidth };
						D2D_RECT_F rc = {};
						std::wstring text = TransferFunction::GetTransferFunctionTextW(layerData.transferType);
						if (cursor)
						{
							lGetTextRect(text, dw, layerInsideTextCursorFormat.Get(), anchor, rc, HorizontalOrientation::Left, VerticalOrientation::Top);
							dc->DrawTextW(text.c_str(), (UINT32)text.size(), layerInsideTextCursorFormat.Get(), rc, _cursorBrush.Get());
						}
						else
						{
							lGetTextRect(text, dw, layerInsideTextFormat.Get(), anchor, rc, HorizontalOrientation::Left, VerticalOrientation::Top);
							dc->DrawTextW(text.c_str(), (UINT32)text.size(), layerInsideTextFormat.Get(), rc, _borderBrush.Get());
						}
					}
					//Drawing text layer
					{
						D2D_POINT_2F anchor = { _renderCanvas.left + 0.5f*layerTextCanvasSize.width, 0.5f*(layerVisual.canvas.bottom + layerVisual.canvas.top) };
						D2D_RECT_F rc = {};
						std::wstringstream wss;
						if (l == 0)
						{
							wss << L"Input";
						}
						else if (l == totalLayers - 1)
						{
							wss << L"Output";
						}
						else
						{
							wss << L"H" << l - 1;
						}
						std::wstring str = wss.str();
						if (cursor)
						{
							lGetTextRect(str, dw, layerTextCursorFormat.Get(), anchor, rc, HorizontalOrientation::Center, VerticalOrientation::Center);
							dc->SetTransform(D2D1::Matrix3x2F::Rotation(-90.0f, anchor));
							dc->DrawTextW(str.c_str(), (UINT32)str.size(), layerTextCursorFormat.Get(), rc, cursor ? _cursorBrush.Get() : _borderBrush.Get());
							dc->SetTransform(D2D1::Matrix3x2F::Identity());
						}
						else
						{
							lGetTextRect(str, dw, layerTextFormat.Get(), anchor, rc, HorizontalOrientation::Center, VerticalOrientation::Center);
							dc->SetTransform(D2D1::Matrix3x2F::Rotation(-90.0f, anchor));
							dc->DrawTextW(str.c_str(), (UINT32)str.size(), layerTextFormat.Get(), rc, cursor ? _cursorBrush.Get() : _borderBrush.Get());
							dc->SetTransform(D2D1::Matrix3x2F::Identity());
						}
					}
				}
				//Drawing weight help text
				if (d->IsHelp() && (pm == ProcessMode::Learned || pm == ProcessMode::Stopped) && firstWeightHelp)
				{
					auto height = layerVisual.canvas.bottom - firstWeightPoint.y;
					wrl::ComPtr<IDWriteTextFormat> helpTextFormat;
					CreateTextFormat(dw, helpTextFormat, 0.7f*height, DWRITE_FONT_STYLE_ITALIC, DWRITE_FONT_WEIGHT_NORMAL);
					//Drawing text
					D2D_RECT_F rc = {};
					std::wstring text = L"Weights";
					D2D_POINT_2F p = {layerVisual.canvas.right - 2.0f*borderStrokeWidth, firstWeightPoint.y};
					lGetTextRect(text, dw, helpTextFormat.Get(), p, rc, HorizontalOrientation::Right, VerticalOrientation::Center);
					dc->DrawTextW(text.c_str(), (UINT32)text.size(), helpTextFormat.Get(), rc, brushes[0].resource.Get());
					//Drawing line
					D2D_POINT_2F pFinal = { rc.left, firstWeightPoint.y };
					dc->DrawLine(firstWeightPoint, pFinal, brushes[0].resource.Get(), helpStrokeWidth, _dashStroke.Get());
				}
				//Setting font
				if (neuralNetworkVisual.minRadius > 0.0f)
				{
					//Drawing neurons
					auto radiusFactor = 0.3f;
					auto radius = radiusFactor * layerVisual.minRadius;
					for (auto n = 0; n < totalNeurons; ++n)
					{
						auto& neuronVisual = layerVisual.neuronVisuals[n];
						auto& neuronData = layerData.neuronDatas[n];
						//Clearing first
						auto& ellipse = D2D1::Ellipse(neuronVisual.p, radius, radius);
						dc->FillEllipse(ellipse, _backBrush.Get());
						//Output
						_outputBrush->SetColor(GetColor(outputCalc, neuronData.output));
						if (pm == ProcessMode::Learning)
						{
							auto opacity = 0.0f;
							auto partial = (float)_outputOpacityTimer.GetPartialTime();
							auto animationTime = 1000.0f;
							if ( partial <= animationTime)
							{
								opacity = 1.0f - (partial / animationTime);
							}
							_outputBrush->SetOpacity(opacity);
						}
						//Neuron and bias geometry
						auto strokeWidth = 0.1f * radius;
						if (l != 0 || n != 0)
						{
							_biasBrush->SetOpacity(1.0f);
							auto biasWeight = neuronData.biasWeight;
							if (pm == ProcessMode::Learning && l < (int)_oldnnd.neuronLayerDatas.size() && n < (int)_oldnnd.neuronLayerDatas[l].neuronDatas.size())
							{
								auto& oldBiasWeight = _oldnnd.neuronLayerDatas[l].neuronDatas[n].biasWeight;
								if (oldBiasWeight != biasWeight)
								{
									_biasBrush->SetOpacity(0.4f);
								}
							}
							_biasBrush->SetColor(GetColor(biasCalc, biasWeight));
							DrawNeuronGeometry(neuronVisual.p, radius, 0.5f * radius, d2d, dc, _biasBrush.Get(), _outputBrush.Get(), _borderBrush.Get(), strokeWidth);
						}
						else
						{
							//neuron for input layer
							dc->FillEllipse(ellipse, _outputBrush.Get());
						}
						//Drawing cursor
						if (cursor)
						{
							dc->DrawEllipse(ellipse, _cursorBrush.Get(), 2.0f * strokeWidth);
						}
						else
						{
							dc->DrawEllipse(ellipse, _borderBrush.Get(), strokeWidth);
						}
						//Helpers
						if (d->IsHelp() && (pm == ProcessMode::Learned || pm == ProcessMode::Stopped) && (l == 0 || l == totalLayers - 1))
						{
							if (l == 0)
							{
								dc->FillEllipse(D2D1::Ellipse(neuronVisual.p, helpBulletRadius, helpBulletRadius), brushes[0].resource.Get());
								//Drawing Input text
								D2D_RECT_F rc = {};
								std::wstring text = L"Input";
								D2D_POINT_2F pText = { layerVisual.canvas.right - 2.0f * borderStrokeWidth, neuronVisual.p.y };
								lGetTextRect(text, dw, layerInsideTextFormat.Get(), pText, rc, HorizontalOrientation::Right, VerticalOrientation::Center);
								dc->DrawTextW(text.c_str(), (UINT32)text.size(), layerInsideTextFormat.Get(), rc, brushes[0].resource.Get());
								//Drawing line
								D2D_POINT_2F p1 = { rc.left, neuronVisual.p.y };
								dc->DrawLine(neuronVisual.p, p1, brushes[0].resource.Get(), helpStrokeWidth, _dashStroke.Get());
								//Drawing output value
								auto y = 0.5f * (0.5f * layerCanvasSize.height - radius);
								pText = { neuronVisual.p.x , layerVisual.canvas.top + y };
								std::wstringstream wss;
								wss << std::setprecision(2) << std::setiosflags(4096) << neuronData.output;
								text = wss.str();
								lGetTextRect(text, dw, layerInsideTextFormat.Get(), pText, rc, HorizontalOrientation::Center, VerticalOrientation::Center);
								dc->DrawTextW(text.c_str(), (UINT32)text.size(), layerInsideTextFormat.Get(), rc, brushes[0].resource.Get());
							}
							else
							{
								D2D_RECT_F rc = {};
								//Drawing output placeholder
								D2D_POINT_2F pOutput = { neuronVisual.p.x, neuronVisual.p.y + 0.5f*radius };
								dc->FillEllipse(D2D1::Ellipse(pOutput, helpBulletRadius, helpBulletRadius), brushes[0].resource.Get());
								//Drawing output text
								D2D_POINT_2F pText = { layerVisual.canvas.right - 2.0f*borderStrokeWidth, pOutput.y };
								std::wstring text = L"Output";
								lGetTextRect(text, dw, layerInsideTextFormat.Get(), pText, rc, HorizontalOrientation::Right, VerticalOrientation::Center);
								dc->DrawTextW(text.c_str(), (UINT32)text.size(), layerInsideTextFormat.Get(), rc, brushes[0].resource.Get());
								//Drawing output line
								D2D_POINT_2F pOutputData = { rc.left, pOutput.y };
								dc->DrawLine(pOutput, pOutputData, brushes[0].resource.Get(), helpStrokeWidth, _dashStroke.Get());
								//Drawing bias placeholder
								D2D_POINT_2F pBias = { neuronVisual.p.x, neuronVisual.p.y - 0.75f*radius };
								dc->FillEllipse(D2D1::Ellipse(pBias, helpBulletRadius, helpBulletRadius), brushes[0].resource.Get());
								//Drawing bias text
								pText = { layerVisual.canvas.right - 2.0f*borderStrokeWidth, pBias.y };
								text = L"Bias";
								lGetTextRect(text, dw, layerInsideTextFormat.Get(), pText, rc, HorizontalOrientation::Right, VerticalOrientation::Center);
								dc->DrawTextW(text.c_str(), (UINT32)text.size(), layerInsideTextFormat.Get(), rc, brushes[0].resource.Get());
								//Drawing bias line
								D2D_POINT_2F pBiasData = { rc.left, pBias.y };
								dc->DrawLine(pBias, pBiasData, brushes[0].resource.Get(), helpStrokeWidth, _dashStroke.Get());
								//Drawing output value
								auto y = 0.5f * (0.5f * layerCanvasSize.height - radius);
								pText = { neuronVisual.p.x , layerVisual.canvas.bottom - y };
								std::wstringstream wss;
								wss << std::setprecision(2) << std::setiosflags(4096) << neuronData.output;
								text = wss.str();
								lGetTextRect(text, dw, layerInsideTextFormat.Get(), pText, rc, HorizontalOrientation::Center, VerticalOrientation::Center);
								dc->DrawTextW(text.c_str(), (UINT32)text.size(), layerInsideTextFormat.Get(), rc, brushes[0].resource.Get());
							}
						}
					}
				}
			}
			_oldnnd = nnd;
			//status area
			D2D_RECT_F statusCanvas = { _renderCanvas.left, engineCanvas.bottom, _renderCanvas.left + statusCanvasSize.width, engineCanvas.bottom + statusCanvasSize.height };
			if (statusCanvasSize.height > 0.0f)
			{
				std::wstring text;
				if (d->IsHelp())
				{
					//Setting text format
					wrl::ComPtr<IDWriteTextFormat> textFormat;
					auto fontHeight = 0.2f * statusCanvasSize.height;
					HR(dw->CreateTextFormat(L"Tahoma", NULL, DWRITE_FONT_WEIGHT_BOLD, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, fontHeight, L"", textFormat.GetAddressOf()));
					textFormat->SetWordWrapping(DWRITE_WORD_WRAPPING_NO_WRAP);
					textFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
					textFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR);
					//Printing data
					std::wstringstream wss;
					wss << lGetProcessMode(d->GetProcessMode()) << std::endl;
					wss << "Epochs/s: " << std::setprecision(2) << std::setiosflags(4096) << (float)d->GetEpochs() << std::endl;
					wss << "Error: " << std::setprecision(2) << std::setiosflags(4096) << d->GetLastError() << std::endl;
					wss << "Step: " << std::setprecision(2) << std::setiosflags(4096) << d->GetStep() << std::endl;
					text = wss.str();
					D2D_POINT_2F p = { statusCanvas.left + layerTextCanvasSize.width, 0.5f * (statusCanvas.top + statusCanvas.bottom) - 2.0f*fontHeight };
					D2D_RECT_F rc = {};
					lGetTextRect(text, dw, textFormat.Get(), p, rc, HorizontalOrientation::Left, VerticalOrientation::Center);
					dc->DrawTextW(text.c_str(), (UINT32)text.size(), textFormat.Get(), rc, brushes[0].resource.Get());
				}
				else
				{
					//Setting text format
					wrl::ComPtr<IDWriteTextFormat> textFormat;
					HR(dw->CreateTextFormat(L"Tahoma", NULL, DWRITE_FONT_WEIGHT_BOLD, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 0.5f * statusCanvasSize.height, L"", textFormat.GetAddressOf()));
					textFormat->SetWordWrapping(DWRITE_WORD_WRAPPING_NO_WRAP);
					textFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
					textFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
					//Drawing text
					text += lGetProcessMode(d->GetProcessMode());
					//Setting color
					auto colorFactor = 1.0f;
					if (pm == ProcessMode::Learning)
					{
						colorFactor = GetColorAnimationFactor();
					}
					_statusBrush->SetOpacity(colorFactor);
					dc->DrawTextW(text.c_str(), (UINT32)text.size(), textFormat.Get(), statusCanvas, _statusBrush.Get());
				}
			}
		}
	};
}