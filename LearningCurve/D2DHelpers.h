#pragma once
#include <wrl.h>
#include <d2d1_1.h>
#include <d3d11_1.h>
#include <dwrite.h>

#pragma comment(lib, "d2d1")
#pragma comment(lib, "d3d11")
#pragma comment(lib, "dxgi")
#pragma comment(lib, "Dwrite")

#pragma warning(disable: 4706)
#pragma warning(disable: 4127)

namespace wrl = Microsoft::WRL;
#if _DEBUG
#define ASSERT(e) _ASSERT(e)
#define VERIFY(expression) ASSERT(expression)
#define HR(expression) ASSERT(S_OK == (expression))

inline void TRACE(WCHAR const* const format, ...)
{
	va_list args;
	va_start(args, format);
	WCHAR output[512] = { L"" };
	vswprintf_s(output, format, args);
	::OutputDebugString(output);
	va_end(args);
}
#else
#define VERIFY(expression) (expression)
#define ASSERT(e) (e)

struct ComException
{
	HRESULT const hr;
	ComException(HRESULT const value) : hr(value) {}
};

inline void HR(HRESULT const hr)
{
	if (S_OK != hr)
		throw ComException(hr);
}

#define TRACE __noop
#endif

static inline HRESULT CreateD3DDeviceByType(D3D_DRIVER_TYPE const type, wrl::ComPtr<ID3D11Device>& device)
{
	ASSERT(!device);

	UINT flags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;

#if _DEBUG
	//flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	return ::D3D11CreateDevice(nullptr, type, nullptr, flags, nullptr, 0, D3D11_SDK_VERSION, device.GetAddressOf(), nullptr, nullptr);
}

static inline wrl::ComPtr<ID3D11Device> CreateD3DDevice()
{
	wrl::ComPtr<ID3D11Device> device;

	auto hr = CreateD3DDeviceByType(D3D_DRIVER_TYPE_HARDWARE, device);
	if (DXGI_ERROR_UNSUPPORTED == hr)
	{
		hr = CreateD3DDeviceByType(D3D_DRIVER_TYPE_WARP, device);
	}
	HR(hr);

	return device;
}

static inline wrl::ComPtr<IDXGIDevice> CreateDXGIDevice(wrl::ComPtr<ID3D11Device> const& d3dDevice)
{
	ASSERT(d3dDevice);

	wrl::ComPtr<IDXGIDevice> dxgiDevice;
	HR(d3dDevice.As(&dxgiDevice));

	return dxgiDevice;
}

static inline wrl::ComPtr<ID2D1Device> CreateD2DDevice(wrl::ComPtr<ID3D11Device> const& d3dDevice)
{
	ASSERT(d3dDevice);

	wrl::ComPtr<IDXGIDevice> dxgiDevice;
	HR(d3dDevice.As(&dxgiDevice));

	wrl::ComPtr<ID2D1Device> d2dDevice;
	HR(::D2D1CreateDevice(dxgiDevice.Get(), nullptr, d2dDevice.GetAddressOf()));

	return d2dDevice;
}

static inline wrl::ComPtr<IDXGIFactory2> CreateDXGIFactory(wrl::ComPtr<ID3D11Device> const& d3dDevice)
{
	ASSERT(d3dDevice);
	wrl::ComPtr<IDXGIDevice> dxgiDevice;
	HR(d3dDevice.As(&dxgiDevice));

	wrl::ComPtr<IDXGIAdapter> dxgiAdapter;
	HR(dxgiDevice->GetAdapter(dxgiAdapter.GetAddressOf()));

	wrl::ComPtr<IDXGIFactory2> dxgiFactory;
	HR(dxgiAdapter->GetParent(__uuidof(dxgiFactory), reinterpret_cast<void**>(dxgiFactory.GetAddressOf())));

	return dxgiFactory;
}

static inline wrl::ComPtr<IDXGISwapChain1> CreateDXGISwapChain(wrl::ComPtr<ID3D11Device> const& d3dDevice, HWND window)
{
	ASSERT(d3dDevice);

	wrl::ComPtr<IDXGIFactory2> const& dxgiFactory = CreateDXGIFactory(d3dDevice);

	DXGI_SWAP_CHAIN_DESC1 props = {};
	props.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	props.SampleDesc.Count = 1;
	props.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	props.BufferCount = 2;
	props.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;

	wrl::ComPtr<IDXGISwapChain1> dxgiSwapChain;
	HR(dxgiFactory->CreateSwapChainForHwnd(d3dDevice.Get(), window, &props, nullptr, nullptr, dxgiSwapChain.GetAddressOf()));

	return dxgiSwapChain;
}

static void SetSwapChainSurfaceToDeviceContext(wrl::ComPtr<IDXGISwapChain1> const& dxgiSwapchain, wrl::ComPtr<ID2D1DeviceContext> const& dc)
{
	ASSERT(dxgiSwapchain);
	ASSERT(dc);

	wrl::ComPtr<IDXGISurface> dxgiSurface;
	HR(dxgiSwapchain->GetBuffer(0, __uuidof(dxgiSurface), reinterpret_cast<void**>(dxgiSurface.GetAddressOf())));

	auto const props = D2D1::BitmapProperties1(D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW, D2D1::PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_IGNORE));

	wrl::ComPtr<ID2D1Bitmap1> bitmap;
	HR(dc->CreateBitmapFromDxgiSurface(dxgiSurface.Get(), props, bitmap.GetAddressOf()));
	dc->SetTarget(bitmap.Get());
}

