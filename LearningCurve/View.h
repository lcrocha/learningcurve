#pragma once
#include "Types.h"
#include "Linear.h"

class View
{
private:
	HWND							_hWnd = 0L;
	D2D_RECT_F						_canvas;
	Index							_screenIndex = 0;
	UI::Screens						_screens;
	//Screens
	UI::Linear						_linear;
	//Factories
	wrl::ComPtr<ID2D1Factory>		_d2dFactory;
	wrl::ComPtr<IDWriteFactory>		_dwFactory;
	//dx devices
	wrl::ComPtr<ID3D11Device>		_d3dDevice;
	wrl::ComPtr<ID2D1Device>		_d2dDevice;
	wrl::ComPtr<IDXGISwapChain1>	_dxgiSwapChain;
	wrl::ComPtr<ID2D1DeviceContext>	_dc;
	//D2d resources
	wrl::ComPtr<IDWriteTextFormat>	_textFormatMedium;
	//Auxilliary
	void CreateDevices(HWND hWnd);
	void ReleaseDevices();
	void ResizeSwapChainSurface();
	bool UpdateCanvas(const D2D_RECT_F& canvas, const Vector& worldCenter, Value maxDistance);
	UI::IScreen* GetCurrentScreen() { return _screens[_screenIndex]; }
	void BrowseScreens(std::function<void(UI::IScreen*)> f);
	//D2D resources
	void CreateDeviceIndependentResources();
	void CreateDeviceResources();
	void CreateDeviceSizeResources();
	void ReleaseDeviceResources();
public:
	View();
	~View();
	//Initialization
	bool Initialize();
	bool Finalize();
	//Renderer functions
	ID2D1Factory*				GetD2DFactory() { return _d2dFactory.Get(); }
	IDWriteFactory*				GetDWriteFactory() { return _dwFactory.Get(); }
	ID2D1DeviceContext*			GetDC() { return _dc.Get(); }
	//Gets and Sets
	const ICoordinateConverter*	GetCoordinateConverter() { return GetCurrentScreen()->GetCoordinateConverter(); }
	const Vector& GetWorldRB() { return GetCoordinateConverter()->GetWorldRB(); }
	const Vector& GetWorldLT() { return GetCoordinateConverter()->GetWorldLT(); }
	//Operations
	bool Render(IData* d);
	//Generics
	//virtual void DrawMessage(const WCHAR* message);
	//Windows messages
	virtual LRESULT OnCreate(HWND hWnd, WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnPaint(HWND hWnd, WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnDestroy(HWND hWnd, WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnSize(HWND hWnd, WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnDisplayChange(HWND hWnd, WPARAM wParam, LPARAM lParam);
};