#pragma once
#include "NeuralNetworkTypes.h"
#include <random>
#include <algorithm>
#include <functional>

class RandomNumberGenerator
{
private:
	std::random_device _rd;
	std::mt19937 _gen;
	RandomNumberGenerator() : _gen(_rd())
	{
	}
public:
	static RandomNumberGenerator& Get()
	{
		static RandomNumberGenerator rng;

		return rng;
	}

	unsigned RandU()
	{
		std::uniform_int_distribution<unsigned int> dist;

		return dist(_gen);
	}

	float RandF()
	{
		/*std::uniform_real_distribution<FPOINT> dist(0.0f, 1.0f);
		return dist(_gen);*/
		return std::generate_canonical<float, 10>(_gen);
	}
};

static float RandFloat()
{
	return RandomNumberGenerator::Get().RandF();
}

static float RandFloatNP()
{
	return -1.0f + 2.0f * RandFloat();
}

static unsigned RandUnsigned()
{
	return RandomNumberGenerator::Get().RandU();
}

static void RandValues(Values& v, std::function<Value(void)> f)
{
	std::generate(v.begin(), v.end(), f);
}

static void RandValues(Values& v)
{
	RandValues(v, RandFloat);
}

static void RandValuesNP(Values& v)
{
	RandValues(v, RandFloatNP);
}

static void RandVectorOfValues(VectorOfValues& vv, Count innerSize, std::function<Value(void)> f)
{
	std::for_each(vv.begin(), vv.end(), [=](Values& v)
	{
		v.resize(innerSize);
		RandValues(v, f);
	});
}

static void RandVectorOfValues(VectorOfValues& vv, Count innerSize)
{
	RandVectorOfValues(vv, innerSize, RandFloat);
}

static void RandVectorOfValuesNP(VectorOfValues& vv, Count innerSize)
{
	RandVectorOfValues(vv, innerSize, RandFloatNP);
}