#pragma once
#include <d2d1.h>
#include "Vector.h"
#include "Types.h"

class CoordinateConverter : public ICoordinateConverter
{
private:
	D2D_RECT_F		_canvas = {};
	FPOINT			_maxDistance = 0.0f;
	D2D_POINT_2F	_viewCenter = {};
	Vector			_worldCenter = {};
	FPOINT			_toViewFactor = 0.0f;
	FPOINT			_toWorldFactor = 0.0f;
	Vector			_wrb = {};
	Vector			_wlt = {};
	//Auxilliary
	bool CheckBoundaries(const D2D_POINT_2F& p1, D2D_POINT_2F& p2) const;
	FPOINT Converter(FPOINT p1, FPOINT c1, FPOINT factor, FPOINT c2, bool invert) const;
	FPOINT ConverterToView(FPOINT worldPoint, FPOINT worldCenter, FPOINT viewCenter, bool invert) const;
	FPOINT ConverterToWorld(FPOINT viewPoint, FPOINT viewCenter, FPOINT worldCenter, bool invert) const;
public:
	CoordinateConverter();
	//Gets and Sets
	FPOINT GetMaxDistance() const { return _maxDistance; }
	const D2D_POINT_2F& GetViewCenter() const { return _viewCenter; }
	const Vector& GetWorldCenter() const { return _worldCenter; }
	const D2D_RECT_F& GetCanvas() const { return _canvas; }
	const Vector& GetWorldRB() const { return _wrb; }
	const Vector& GetWorldLT() const { return _wlt; }
	//Operations
	void Update(const D2D_POINT_2F& viewCenter, const Vector& worldCenter, FPOINT maxWorldDistance, const D2D_RECT_F& canvas);
	void UpdateMaxDistance(FPOINT maxDistance);
	FPOINT ConvertLengthToView(FPOINT length) const;
	bool ConvertWorldToView(const Vector& v, D2D1_POINT_2F& p) const;
	bool ConvertViewToWorld(const D2D1_POINT_2F& p, Vector& v) const;
};