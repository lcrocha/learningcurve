#pragma once
#include "Screen.h"
#include "Canvas.h"
#include "Lines.h"
#include "Graph.h"
#include "DesiredPoints.h"
#include "LocalPoint.h"
#include "Network.h"
#include "Legend.h"

namespace UI
{
	class Linear : public IScreen
	{
	private:
		//Elements
		Canvas				_canvas;
		Lines				_lines;
		Graph				_graph;
		DesiredPoints		_desiredPoints;
		Network				_network;
		LocalPoint			_localPoint;
		Legend				_legend;

	public:
		Linear()
		{
			//Screen elements
			_elements.push_back(&_canvas);
			_elements.push_back(&_lines);
			_elements.push_back(&_graph);
			_elements.push_back(&_desiredPoints);
			_elements.push_back(&_network);
			_elements.push_back(&_localPoint);
			_elements.push_back(&_legend);
			//Shared brushes
			_brushes.push_back(Brush(D2D1::ColorF::Gold)); //Help Data
		}

		virtual ~Linear()
		{
		}

		virtual void UpdateCanvas(const D2D_RECT_F& renderCanvas, const Vector& worldCenter, Value maxDistance)
		{
			//Total render canvas
			D2D_SIZE_F renderCanvasSize = { renderCanvas.right - renderCanvas.left, renderCanvas.bottom - renderCanvas.top };
			//Network canvas
			auto factor = 0.25f;
			D2D_SIZE_F networkCanvasSize = { factor * renderCanvasSize.width,  renderCanvasSize.height };
			D2D_RECT_F networkCanvas = { renderCanvas.right - networkCanvasSize.width, renderCanvas.top, renderCanvas.right, renderCanvas.top + networkCanvasSize.height};
			//Graph canvas
			D2D_SIZE_F graphCanvasSize = { (1.0f - factor) * renderCanvasSize.width,  renderCanvasSize.height };
			D2D_RECT_F graphCanvas = { renderCanvas.left, renderCanvas.top, renderCanvas.left + graphCanvasSize.width, renderCanvas.top + graphCanvasSize.height };
			//Setting coordinate converter
			D2D_POINT_2F viewCenter = { graphCanvasSize.width / 2.0f, graphCanvasSize.height / 2.0f };
			_cc.Update(viewCenter, worldCenter, maxDistance, graphCanvas);
			//Setting layout
			_canvas.UpdateCanvas(renderCanvas);
			_lines.UpdateCanvas(graphCanvas);
			_graph.UpdateCanvas(graphCanvas);
			_desiredPoints.UpdateCanvas(graphCanvas);
			_network.UpdateCanvas(networkCanvas);
			_localPoint.UpdateCanvas(graphCanvas);
			_legend.UpdateCanvas(graphCanvas);
		}
	};
}
