#pragma once
#include <chrono>

class ChronoTimer
{
private:
	std::chrono::time_point<std::chrono::system_clock> _start;
	std::chrono::time_point<std::chrono::system_clock> _stop;

	unsigned long GetTime(std::chrono::time_point<std::chrono::system_clock>& t)
	{
		return (unsigned long)std::chrono::duration_cast<std::chrono::milliseconds>(t - _start).count();
	}

public:
	ChronoTimer()
	{
		Start();
	}

	void Start()
	{
		_start = std::chrono::system_clock::now();
	}

	ChronoTimer& Stop()
	{
		_stop = std::chrono::system_clock::now();

		return *this;
	}

	unsigned long GetTime()
	{
		return GetTime(_stop);
	}

	unsigned long GetPartialTime()
	{
		return GetTime(std::chrono::system_clock::now());
	}
};
