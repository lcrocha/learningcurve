#pragma once

namespace UI
{
	class Graph : public IElement
	{
	private:
		wrl::ComPtr<ID2D1SolidColorBrush>	_brush;

	public:
		virtual void CreateDeviceIndependentResources(ID2D1Factory* d2d, IDWriteFactory* dw)
		{}

		virtual void CreateDeviceResources(ID2D1DeviceContext* dc)
		{
			HR(dc->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::White), _brush.ReleaseAndGetAddressOf()));
		}

		virtual void CreateDeviceSizeResources(ID2D1DeviceContext* dc)
		{}

		virtual void ReleaseDeviceResources()
		{
			_brush.Reset();
		}

		virtual void Render(ID2D1Factory* d2d, IDWriteFactory* dw, ID2D1DeviceContext* dc, ICoordinateConverter* cc, IData* d, Brushes& brushes)
		{
			auto& graphPoints = d->GetGraphPoints();
			//Browsing data
			auto first = true;
			D2D_POINT_2F lastPoint = {};
			std::for_each(graphPoints.begin(), graphPoints.end(), [&](const Vector& v)
			{
				D2D_POINT_2F p;
				//Converting
				cc->ConvertWorldToView(v, p);
				if (first)
				{
					first = false;
				}
				else
				{
					dc->DrawLine(lastPoint, p, _brush.Get());
				}
				lastPoint = p;
			});
		}
	};
}