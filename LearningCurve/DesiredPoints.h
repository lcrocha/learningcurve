#pragma once

namespace UI
{
	class DesiredPoints : public IElement
	{
	private:
		wrl::ComPtr<ID2D1SolidColorBrush> _brush;

	public:
		virtual void CreateDeviceIndependentResources(ID2D1Factory* d2d, IDWriteFactory* dw)
		{}

		virtual void CreateDeviceResources(ID2D1DeviceContext* dc)
		{
			HR(dc->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Yellow), _brush.ReleaseAndGetAddressOf()));
		}

		virtual void CreateDeviceSizeResources(ID2D1DeviceContext* dc)
		{}

		virtual void ReleaseDeviceResources()
		{
			_brush.Reset();
		}

		virtual void Render(ID2D1Factory* d2d, IDWriteFactory* dw, ID2D1DeviceContext* dc, ICoordinateConverter* cc, IData* d, Brushes& brushes)
		{
			Vectors desireds;
			d->GetDesiredPoints(desireds);
			//Browsing data
			std::for_each(desireds.begin(), desireds.end(), [&](const Vector& v)
			{
				D2D_POINT_2F p;
				//Converting
				cc->ConvertWorldToView(v, p);
				if (p.x < _renderCanvas.right)
				{
					//Drawing point
					dc->FillEllipse(D2D1::Ellipse(p, 3, 3), _brush.Get());
				}
			});
		}
	};
}