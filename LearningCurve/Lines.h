#pragma once
#include <iomanip>
#include <sstream>

namespace UI
{
	class Lines : public IElement
	{
		wrl::ComPtr<ID2D1SolidColorBrush>	_axisBrush;

	public:
		virtual void CreateDeviceIndependentResources(ID2D1Factory* d2d, IDWriteFactory* dw)
		{
		}

		virtual void CreateDeviceResources(ID2D1DeviceContext* dc)
		{
			HR(dc->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Gray), _axisBrush.ReleaseAndGetAddressOf()));
		}

		virtual void CreateDeviceSizeResources(ID2D1DeviceContext* dc)
		{}

		virtual void ReleaseDeviceResources()
		{
			_axisBrush.Reset();
		}

		virtual void Render(ID2D1Factory* d2d, IDWriteFactory* dw, ID2D1DeviceContext* dc, ICoordinateConverter* cc, IData* d, Brushes& brushes)
		{
			std::wstringstream wss;
			D2D_POINT_2F lt = {}, rb = {};
			auto& wlt = cc->GetWorldLT();
			auto& wrb = cc->GetWorldRB();
			cc->ConvertWorldToView(wlt, lt);
			cc->ConvertWorldToView(wrb, rb);
			//Setting axis
			Vector x1(wlt.x, 0.0f);
			Vector x2(wrb.x, 0.0f);
			Vector y1(0.0f, wlt.y);
			Vector y2(0.0f, wrb.y);
			D2D_POINT_2F p1 = {}, p2 = {};
			//Converting
			cc->ConvertWorldToView(x1, p1);
			cc->ConvertWorldToView(x2, p2);
			//Drawing x-axis
			dc->DrawLine(p1, p2, _axisBrush.Get());
			//Converting
			cc->ConvertWorldToView(y1, p1);
			cc->ConvertWorldToView(y2, p2);
			//Drawing y-axis
			dc->DrawLine(p1, p2, _axisBrush.Get());
			//Drawing ruler
			auto maxDistance = cc->GetMaxDistance();
			auto scale = (FPOINT)::powf(10.0f, (FPOINT)(Count)::logf(maxDistance));
			auto size = 5;
			//D2D Objects
			wrl::ComPtr<IDWriteTextFormat> textFormat;
			HR(dw->CreateTextFormat(L"Tahoma", NULL, DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 0.005f*_renderCanvasSize.width, L"", textFormat.GetAddressOf()));
			textFormat->SetWordWrapping(DWRITE_WORD_WRAPPING_NO_WRAP);
			textFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
			textFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR);
			//Points and rects
			D2D1_POINT_2F p = {};
			D2D_RECT_F rc = {};
			//Drawing x
			auto i = 0;
			Vector v = { 0.0f, 0.0f };
			while (v.x < wrb.x)
			{
				i++;
				v.x += 0.1f * scale;
				if (v.x < wrb.x)
				{
					if (i == 10)
					{
						size *= 2;
					}
					//Positive X
					cc->ConvertWorldToView(Vector(v.x, 0.0f), p);
					p1 = { p.x, p.y - size };
					p2 = { p.x, p.y + size };
					dc->DrawLine(p1, p2, _axisBrush.Get());
					//Drawing number
					wss.str(L"");
					wss << std::setprecision(2) << std::fixed << v.x;
					lGetTextRect(wss.str(), dw, textFormat.Get(), p1, rc, HorizontalOrientation::Center, VerticalOrientation::Bottom);
					dc->DrawTextW(wss.str().c_str(), (UINT32)wss.str().size(), textFormat.Get(), rc, _axisBrush.Get());
					//Negative X
					cc->ConvertWorldToView(Vector(-v.x, 0.0f), p);
					p1 = { p.x, p.y - size };
					p2 = { p.x, p.y + size };
					dc->DrawLine(p1, p2, _axisBrush.Get());
					//Drawing number
					wss.str(L"");
					wss << std::setprecision(2) << std::fixed << -v.x;
					lGetTextRect(wss.str(), dw, textFormat.Get(), p2, rc, HorizontalOrientation::Center, VerticalOrientation::Top);
					dc->DrawTextW(wss.str().c_str(), (UINT32)wss.str().size(), textFormat.Get(), rc, _axisBrush.Get());
					if (i == 10)
					{
						size /= 2;
					}
				}
			}
			//Drawing y
			i = 0;
			v = { 0.0f, 0.0f };
			while (v.y < wlt.y)
			{
				i++;
				v.y += 0.1f * scale;
				if (v.y < wlt.y)
				{
					if (i == 10)
					{
						size *= 2;
					}
					//Positive Y
					cc->ConvertWorldToView(Vector(0.0f, v.y), p);
					p1 = { p.x - size, p.y };
					p2 = { p.x + size, p.y };
					dc->DrawLine(p1, p2, _axisBrush.Get());
					//Drawing number
					wss.str(L"");
					wss << std::setprecision(2) << std::fixed << v.y;
					lGetTextRect(wss.str(), dw, textFormat.Get(), p1, rc, HorizontalOrientation::Right, VerticalOrientation::Center);
					dc->DrawTextW(wss.str().c_str(), (UINT32)wss.str().size(), textFormat.Get(), rc, _axisBrush.Get());
					//Negative Y
					cc->ConvertWorldToView(Vector(0.0f, -v.y), p);
					p1 = { p.x - size, p.y };
					p2 = { p.x + size, p.y };
					dc->DrawLine(p1, p2, _axisBrush.Get());
					//Drawing number
					wss.str(L"");
					wss << std::setprecision(2) << std::fixed << -v.y;
					lGetTextRect(wss.str(), dw, textFormat.Get(), p2, rc, HorizontalOrientation::Left, VerticalOrientation::Center);
					dc->DrawTextW(wss.str().c_str(), (UINT32)wss.str().size(), textFormat.Get(), rc, _axisBrush.Get());
					if (i == 10)
					{
						size /= 2;
					}
				}
			}
		}
	};
}