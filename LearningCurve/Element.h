#pragma once
#include "Types.h"
#include "D2DHelpers.h"

namespace UI
{
	class IElement
	{
	protected:
		D2D_RECT_F	_renderCanvas = {};
		D2D1_SIZE_F	_renderCanvasSize = {};
	public:
		//D2D chain
		virtual void CreateDeviceIndependentResources(ID2D1Factory*, IDWriteFactory*) = 0;
		virtual void CreateDeviceResources(ID2D1DeviceContext*) = 0;
		virtual void CreateDeviceSizeResources(ID2D1DeviceContext*) = 0;
		virtual void ReleaseDeviceResources() = 0;
		//Operations
		virtual void UpdateCanvas(const D2D_RECT_F& renderCanvas)
		{
			_renderCanvas = renderCanvas;
			_renderCanvasSize = { renderCanvas.right - renderCanvas.left, renderCanvas.bottom - renderCanvas.top };
		}

		virtual void Render(ID2D1Factory*, IDWriteFactory*, ID2D1DeviceContext*, ICoordinateConverter*, IData*, Brushes&) = 0;
	};

	typedef std::vector<IElement*>		Elements;
}
