#pragma once
#include "NeuralNetwork.h"
#include "Vector.h"
#include <d2d1_1.h>

enum class ProcessMode
{
	Stopped,
	Learning,
	Learned
};

class IData
{
public:
	virtual const Values& GetLocalInput() const = 0;
	virtual void SetLocalInput(const Values& input) = 0;
	virtual const Values& GetLinearInputs() = 0;
	virtual Value GetLastError() = 0;
	virtual void SetLastError(Value e) = 0;
	virtual Vectors& GetGraphPoints() = 0;
	virtual void GetDesiredPoints(Vectors& desireds) const = 0;;
	virtual const InputsOutputs& GetTrainset() const = 0;
	virtual const ProcessMode& GetProcessMode() const = 0;
	virtual const Vector& GetWorldCenter() const = 0;
	virtual Value GetMaxDistance() const = 0;
	virtual void SetMaxDistance(Value d) = 0;
	virtual Count GetEpochs() = 0;
	virtual Value GetFPS() = 0;
	virtual void DisableLocalInput() = 0;
	virtual const NeuralNetworkData& GetNeuralNetworkData() const = 0;
	virtual void SetNeuralNetworkData(const NeuralNetworkData& nnd) = 0;
	virtual void SetHiddenLayerCount(Count c) = 0;
	virtual Count GetLayerCursor() const = 0;
	virtual Value GetStep() const = 0;
	virtual void SetStep(Value s) = 0;
	virtual Value GetDesiredError() const = 0;
	virtual Counts& GetDynamicLearningRate() = 0;
	virtual bool IsHelp() = 0;
};

class ICoordinateConverter
{
public:
	virtual FPOINT GetMaxDistance() const = 0;
	virtual const Vector& GetWorldRB() const = 0;
	virtual const Vector& GetWorldLT() const = 0;
	virtual bool ConvertWorldToView(const Vector& v, D2D1_POINT_2F& p) const = 0;
	virtual bool ConvertViewToWorld(const D2D1_POINT_2F& p, Vector& v) const = 0;
};