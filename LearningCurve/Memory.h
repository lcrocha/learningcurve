#pragma once
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#ifdef _DEBUG
#define SAFE_NEW(p)			new (_NORMAL_BLOCK, __FILE__, __LINE__) p
#define SAFE_MALLOC(s)		::_malloc_dbg(s, _CLIENT_BLOCK, __FILE__, __LINE__)
#define SAFE_CALLOC(n,s)	::_calloc_dbg(n, s, _CLIENT_BLOCK, __FILE__, __LINE__)
#define SAFE_REALLOC(p,n)	::_realloc_dbg( p, n, _CLIENT_BLOCK, __FILE__, __LINE__)
#else
#define SAFE_NEW(p)			new p
#define SAFE_MALLOC(s)		::malloc(s)
#define SAFE_CALLOC(n,s)	::calloc(n,s)
#define SAFE_REALLOC(p,n)	::realloc(p,n)
#endif

#define SAFE_DELETE(p) {if(nullptr != p) {delete p; p = nullptr;}}

class MemoryWatcher
{
public:
	MemoryWatcher()
	{
		_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	}

	virtual ~MemoryWatcher()
	{
		_CrtDumpMemoryLeaks();
	}
};