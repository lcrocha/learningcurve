#pragma once
#include "UI.h"
#include "Element.h"
#include "CoordinateConverter.h"

namespace UI
{
	class IScreen
	{
	protected:
		Elements			_elements;
		Brushes				_brushes;
		CoordinateConverter	_cc;
		//Auxilliary
		void BrowseElements(std::function<void(IElement*)> f) const
		{
			std::for_each(_elements.begin(), _elements.end(), f);
		}

		void BrowseBrushes(std::function<void(Brush& b)> f)
		{
			std::for_each(_brushes.begin(), _brushes.end(), f);
		}
	public:
		IScreen() {}
		virtual ~IScreen()
		{
			_elements.clear();
		}

		//D2D chain
		virtual void CreateDeviceIndependentResources(ID2D1Factory* d2d, IDWriteFactory* dw)
		{
			BrowseElements([&](IElement* element) { element->CreateDeviceIndependentResources(d2d, dw); });
		}

		virtual void CreateDeviceResources(ID2D1DeviceContext* dc)
		{
			BrowseBrushes([&](Brush& b) {b.CreateDeviceResources(dc); });
			BrowseElements([&](IElement* element) { element->CreateDeviceResources(dc); });
		}

		virtual void CreateDeviceSizeResources(ID2D1DeviceContext* dc)
		{
			BrowseElements([&](IElement* element) { element->CreateDeviceSizeResources(dc); });
		}

		virtual void ReleaseDeviceResources()
		{
			BrowseBrushes([&](Brush& b) {b.ReleaseDeviceResources(); });
			BrowseElements([](IElement* element) { element->ReleaseDeviceResources(); });
		}

		//Operations
		virtual const ICoordinateConverter* GetCoordinateConverter() const { return &_cc; }

		virtual void UpdateCanvas(const D2D_RECT_F& canvas, const Vector& worldCenter, Value maxDistance) = 0;

		virtual void Render(ID2D1Factory* d2d, IDWriteFactory* dw, ID2D1DeviceContext* dc, IData* d)
		{
			BrowseElements([&](IElement* element) { element->Render(d2d, dw, dc, &_cc, d, _brushes); });
		}
	};

	typedef std::vector<IScreen*>	Screens;
};