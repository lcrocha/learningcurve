#pragma once
#include "Types.h"
#include "Data.h"
#include "Processor.h"
#include "View.h"

class Engine
{
private:
	Data				_d;
	Processor			_p;
	View				_v;
public:
	Engine();
	~Engine();
	//Initialization
	bool Initialize();
	bool Finalize();
	//Operations
	void AddDesiredPoint(const Values& input, const Values& output);
	bool DeleteLastDesiredPoint() { return _d.DeleteLastDesiredPoint(); }
	bool DoLearn();
	bool UpdateGraphPoints();
	bool UpdateNeuralNetworkData();
	bool DoRender();
	bool ResetStep();
	bool Randomize();
	void SetLocalInput(const Values& input) { _d.SetLocalInput(input); }
	bool SetInputs(Value min, Value max, Count steps) { return _d.SetInputs(min, max, steps); }
	void SetEpoch(Count e) { _d.SetEpochs(e); }
	void SetFPS(Value fps) { _d.SetFPS(fps); }
	bool DoLayerCursorUp() { return _d.DoLayerCursorUp(); }
	bool DoLayerCursorDown() { return _d.DoLayerCursorDown(); }
	bool AddNeuronLayer();
	bool DeleteNeuronLayer();
	bool IncreaseNeuron();
	bool DecreaseNeuron();
	bool ChangeTransferFunctionType();
	bool ToggleHelp();
	//Gets and Sets
	ProcessMode GetProcessMode() { return _d.GetProcessMode(); }
	void SetProcessMode(ProcessMode pm) { _d.SetProcessMode(pm); }
	void SetDesiredError(Value e) { _d.SetDesiredError(e); }
	//Custom events
	void OnMouseClick(D2D_POINT_2F& p);
	void OnMouseMove(D2D_POINT_2F& p);
	//Windows messages
	virtual LRESULT OnCreate(HWND hWnd, WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnPaint(HWND hWnd, WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnDestroy(HWND hWnd, WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnSize(HWND hWnd, WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnDisplayChange(HWND hWnd, WPARAM wParam, LPARAM lParam);
};

