#include "Processor.h"
#include "RandomNumberGenerator.h"
#include "Vector.h"

Processor::Processor() : _e(NewNeuralNetworkEngineInstance(), ReleaseNeuralNetworkEngineInstance)
{
}

Processor::~Processor()
{
}

bool Processor::Initialize(IData* d)
{
	auto costType = CostFunctionType::RootMeanSquare;
	LayersConfig lcs;
	//Creating NNE
	auto inputLayerNeurons = 1;
	auto outputLayerNeurons = 1;
	auto hiddenLayers = RandUnsigned() % 10 + 1;
	lcs.push_back(LayerConfig(inputLayerNeurons, TransferFunctionType::Linear, 0.0f));
	for (auto i = 0; i < (int)hiddenLayers; ++i)
	{
		lcs.push_back(LayerConfig(RandUnsigned() % 10 + 1, GetRandomTransferFunction()));
	}
	lcs.push_back(LayerConfig(outputLayerNeurons, TransferFunctionType::Linear));
	//BackPropagation configuration 
	BackPropagationConfig bc(0.02f, 1e-9f, 0.0f);
	//Creating neuralnetwork
	_e->Create(costType, lcs, bc);
	d->SetHiddenLayerCount((Count)lcs.size() - 2);

	return false;
}

bool Processor::Finalize()
{
	_e->Destroy();

	return false;
}

bool Processor::Randomize()
{
	return _e->Randomize();
}

bool Processor::ResetStep()
{
	return _e->ResetStep();
}

bool Processor::DoLearn(IData* d)
{
	//Learning
	auto error = _e->Learn(d->GetTrainset(), true, d->GetDynamicLearningRate());
	d->SetLastError(error);

	return error <= d->GetDesiredError();
}

bool Processor::UpdateNeuralNetworkData(IData* d)
{
	bool localActive = false;
	InputOutput local;
	//Getting local point
	local.input = d->GetLocalInput();
	if (local.input.size() > 0)
	{
		_e->ForwardPropagation(local.input, local.output);
		localActive = true;
	}
	//Getting nne data
	NeuralNetworkData nnd;
	_e->GetNeuralNetworkData(nnd);
	//Filling graphpoints
	if (localActive)
	{
		nnd.local = local;
	}
	nnd.localActive = localActive && d->GetProcessMode() != ProcessMode::Learning;
	d->SetNeuralNetworkData(nnd);

	return false;
}

bool Processor::UpdateGraphPoints(IData* d)
{
	//Filling graphpoints
	auto& graphPoints = d->GetGraphPoints();
	auto& linearInputs = d->GetLinearInputs();
	auto size = (Count)linearInputs.size();
	//Transforming inputs
	VectorOfValues vvi(size), vvo(size);
	std::transform(linearInputs.begin(), linearInputs.end(), vvi.begin(), [&](const Value v)
	{
		return Values() = { v };
	});
	//Applying forward propagation
	_e->ForwardPropagation(vvi, vvo);
	//Writing output
	for (Count i = 0; i < size; ++i)
	{
		graphPoints[i].x = linearInputs[i];
		graphPoints[i].y = vvo[i][0];/**/
		/*graphPoints[i].x = vvo[i][0];
		graphPoints[i].y = vvo[i][1];/**/
	}
	d->SetStep(_e->GetStep());

	return true;
}

bool Processor::AddNeuronLayer(IData* d)
{
	auto ret = _e->AddNeuronLayer(d->GetLayerCursor() + 1);
	d->SetHiddenLayerCount(_e->GetNeuronLayersCount() - 2);

	return ret;
}

bool Processor::DeleteNeuronLayer(IData* d)
{
	auto ret = false;
	if (_e->GetNeuronLayersCount() > 3)
	{
		ret = _e->DeleteNeuronLayer(d->GetLayerCursor() + 1);
		d->SetHiddenLayerCount(_e->GetNeuronLayersCount() - 2);
	}

	return ret;
}

TransferFunctionType Processor::GetRandomTransferFunction()
{
	auto ret = TransferFunctionType();
	switch (RandUnsigned() % 4)
	{
	case 0:
		ret = TransferFunctionType::ReLU;
		break;
	case 1:
		ret = TransferFunctionType::TanH;
		break;
	case 2:
		ret = TransferFunctionType::Logistic;
		break;
	case 3:
		ret = TransferFunctionType::Square;
		break;
	}

	return ret;
}

bool Processor::ChangeTransferFunctionType(IData* d)
{
	auto ret = false;
	auto cursor = d->GetLayerCursor() + 1;
	TransferFunctionType t;
	if (_e->GetTransferFunctionType(cursor, t))
	{
		//Changing type
		if (t == TransferFunctionType::ReLU)
		{
			t = TransferFunctionType::TanH;
		}
		else if(t == TransferFunctionType::TanH)
		{
			t = TransferFunctionType::Logistic;
		}
		else if (t == TransferFunctionType::Logistic)
		{
			t = TransferFunctionType::Square;
		}
		else if (t == TransferFunctionType::Square)
		{
			t = TransferFunctionType::ReLU;
		}
		ret = _e->SetTransferFunctionType(cursor, t);
	}

	return ret;
}