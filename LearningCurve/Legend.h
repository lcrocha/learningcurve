#pragma once
#include <iomanip>

#define FP std::setprecision(2) << std::setiosflags(4096) << std::setw(9)

namespace UI
{
	class Legend : public IElement
	{
	private:
		wrl::ComPtr<IDWriteTextFormat>		_textFormat;

	public:
		virtual void CreateDeviceIndependentResources(ID2D1Factory* d2d, IDWriteFactory* dw)
		{
			//Creating text format
			HR(dw->CreateTextFormat(L"Tahoma", NULL, DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 10.0f, L"", _textFormat.GetAddressOf()));
			_textFormat->SetWordWrapping(DWRITE_WORD_WRAPPING_NO_WRAP);
			_textFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_JUSTIFIED);
			_textFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR);
		}

		virtual void CreateDeviceResources(ID2D1DeviceContext* dc)
		{
		}

		virtual void CreateDeviceSizeResources(ID2D1DeviceContext* dc)
		{}

		virtual void ReleaseDeviceResources()
		{
		}

		virtual void Render(ID2D1Factory* d2d, IDWriteFactory* dw, ID2D1DeviceContext* dc, ICoordinateConverter* cc, IData* d, Brushes& brushes)
		{
			D2D1_RECT_F rc = {};
			D2D_POINT_2F p = { 20, 20 };
			const auto& nnd = d->GetNeuralNetworkData();
			//auto& dlr = d->GetDynamicLearningRate();
			//Creating text format
			wrl::ComPtr<IDWriteTextFormat> textFormat;
			CreateTextFormat(dw, textFormat, 0.02f* _renderCanvasSize.height, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_WEIGHT_NORMAL);
			textFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
			textFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR);
			//Setting text
			std::wstringstream oss;
			if (d->IsHelp())
			{
				oss << "<Up> <Down>\tMove Hidden Layer cursor" << std::endl;
				oss << "<Left> <Right>\tInsert/Delete neuron" << std::endl;
				oss << "<a, A>\t\tInserts a new Hidden Layer" << std::endl;
				oss << "<d, D>\t\tDeletes the current Hidden Layer" << std::endl;
				oss << "<t, T>\t\tChanges the Transfer Function" << std::endl;
				oss << "<r, R>\t\tRandomize weights and biases" << std::endl;
				oss << "Mouse Left-click\tCreates a desired point" << std::endl;
				oss << "<Delete>\tDeletes a desired point" << std::endl;
			}
			else
			{
				oss << "Press <H> for Help" << std::endl;
			}
			/*if(dlr.size() == 3)
				oss << "[ " << dlr[0] << ", " << dlr[1] << ", " << dlr[2] << "]" << std::endl;*/
			auto& text = oss.str();
			lGetTextRect(text, dw, textFormat.Get(), p, rc, HorizontalOrientation::Left, VerticalOrientation::Top);
			dc->DrawText(text.c_str(), (UINT32)text.size(), textFormat.Get(), rc, brushes[0].resource.Get());
		}
	};
}