#include "View.h"

View::View()
{
}

View::~View()
{
}

bool View::Initialize()
{
	_screens.push_back(&_linear);

	return false;
}

bool View::Finalize()
{
	return false;
}

void View::BrowseScreens(std::function<void(UI::IScreen*)> f)
{
	std::for_each(_screens.begin(), _screens.end(), f);
}

bool View::UpdateCanvas(const D2D_RECT_F& canvas, const Vector& worldCenter, Value maxDistance)
{
	BrowseScreens([&](UI::IScreen* s) {s->UpdateCanvas(canvas, worldCenter, maxDistance); });

	return false;
}

LRESULT View::OnCreate(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	_hWnd = hWnd;
	CreateDeviceIndependentResources();

	return LRESULT();
}

LRESULT View::OnPaint(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	return LRESULT();
}

LRESULT View::OnDestroy(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	ReleaseDevices();
	_d2dDevice.Reset();
	_d3dDevice.Reset();

	return LRESULT();
}

LRESULT View::OnSize(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	RECT canvas = {};
	::GetClientRect(hWnd, &canvas);
	_canvas = { (float)canvas.left, (float)canvas.top, (float)canvas.right, (float)canvas.bottom };
	if (SIZE_MINIMIZED != wParam)
	{
		UpdateCanvas(_canvas, Vector(), 1.05f);
		if (_dc)
		{
			ResizeSwapChainSurface();
		}
	}
	//DrawMessage(L"Resizing...");

	return LRESULT();
}

LRESULT View::OnDisplayChange(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	return LRESULT();
}

void View::CreateDevices(HWND hWnd)
{
	if (!_dc)
	{
		//Swap Chain
		_dxgiSwapChain = CreateDXGISwapChain(_d3dDevice, hWnd);
		//Direct2d device context
		HR(_d2dDevice->CreateDeviceContext(D2D1_DEVICE_CONTEXT_OPTIONS_NONE, _dc.GetAddressOf()));
		SetSwapChainSurfaceToDeviceContext(_dxgiSwapChain, _dc);
		CreateDeviceResources();
		CreateDeviceSizeResources();
	}
}

void View::ReleaseDevices()
{
	if (_dc)
	{
		ReleaseDeviceResources();
		_dc.Reset();
		_dxgiSwapChain.Reset();
	}
}

void View::ResizeSwapChainSurface()
{
	ASSERT(_dc);
	ASSERT(_dxgiSwapChain);

	_dc->SetTarget(nullptr);
	if (S_OK == _dxgiSwapChain->ResizeBuffers(0, (UINT)(_canvas.right - _canvas.left), (UINT)(_canvas.bottom - _canvas.top), DXGI_FORMAT_UNKNOWN, 0))
	{
		SetSwapChainSurfaceToDeviceContext(_dxgiSwapChain, _dc);
		CreateDeviceSizeResources();
	}
	else
	{
		ReleaseDevices();
	}
}

void View::CreateDeviceIndependentResources()
{
	//Direct3d device
	_d3dDevice = CreateD3DDevice();
	//Direct2d device
	_d2dDevice = CreateD2DDevice(_d3dDevice);
	_d2dDevice->GetFactory(_d2dFactory.GetAddressOf());
	//Creating DWrite factory
	HR(::DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(_dwFactory.Get()), reinterpret_cast<IUnknown**>(_dwFactory.GetAddressOf())));
	//Creating independent resources on elements
	BrowseScreens([&](UI::IScreen* s) {s->CreateDeviceIndependentResources(_d2dFactory.Get(), _dwFactory.Get()); });
	//Text medium
	HR(_dwFactory->CreateTextFormat(L"Tahoma", NULL, DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 25.0f, L"", _textFormatMedium.GetAddressOf()));
	_textFormatMedium->SetWordWrapping(DWRITE_WORD_WRAPPING_NO_WRAP);
	_textFormatMedium->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
	_textFormatMedium->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
}

void View::CreateDeviceResources()
{
	BrowseScreens([&](UI::IScreen* s) {s->CreateDeviceResources(_dc.Get()); });
}

void View::CreateDeviceSizeResources()
{
	BrowseScreens([&](UI::IScreen* s) {s->CreateDeviceSizeResources(_dc.Get()); });
}

void View::ReleaseDeviceResources()
{
	BrowseScreens([&](UI::IScreen* s) {s->ReleaseDeviceResources(); });
}

bool View::Render(IData * d)
{
	//Creating devices if needed
	CreateDevices(_hWnd);
	_dc->BeginDraw();
	GetCurrentScreen()->Render(_d2dFactory.Get(), _dwFactory.Get(), _dc.Get(), d);
	auto hr = _dc->EndDraw();
	if (D2DERR_RECREATE_TARGET == hr)
	{
		ReleaseDevices();
	}
	//Presenting
	_dxgiSwapChain->Present(1, 0);

	return false;
}

/*void View::DrawMessage(const WCHAR* message)
{
	std::wstring text(message);
	//Creating devices if needed
	CreateDevices(_hWnd);
	//Begin Draw
	_dc->BeginDraw();
	//Drawing text
	ClearCanvas();
	auto sz = D2D1::SizeF((FPOINT)_drawCanvas->right - (FPOINT)_drawCanvas->left, (FPOINT)_drawCanvas->bottom - (FPOINT)_drawCanvas->top);
	//auto rc = d2d::RectF(sz.width / 2, sz.height, sz.width / 2, sz.height / 2);
	D2D1_RECT_F rc = {};
	lGetTextRect(text, _dwFactory.Get(), _textFormatMedium.Get(), D2D1::Point2F(sz.width / 2.0f, sz.height / 2.0f), rc, true);
	wrl::ComPtr<ID2D1SolidColorBrush> brush;
	HR(_dc->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::White), brush.ReleaseAndGetAddressOf()));
	_dc->DrawText(text.c_str(), (UINT32)text.size(), _textFormatMedium.Get(), rc, brush.Get());
	//End drawing
	auto hr = _dc->EndDraw();
	if (D2DERR_RECREATE_TARGET == hr)
	{
		ReleaseDevices();
	}
	//Presenting
	_dxgiSwapChain->Present(1, 0);
}*/