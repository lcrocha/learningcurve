# Neural Network with Backpropagation   

**Tags**: C++11, Math, Direct2D, DirectWrite, Neural Network, Backpropagtion, Perceptrons

> This is my Neural Network with Backpropagation algorithm implementation.

This simulation uses one *Input* (x coord) and one *Output* (y coord). *Mouse Left Clicking* on the screen a new desired point is created and when in Learning mode (*SpaceBar*) the graph will try to reach that point. If it is not converging add more neurons per layers and layers.  

## Commads:  
-   a, A:			Add a hidden layer  
-   d, D:			Delete the current hidden layer  
-   *UP*: 			Move hidden layer cursor UP  
-   *DOWN*:			Move hidden layer cursor DOWN  
-   *LEFT*:			Delete a neuron on the current hidden - layer  
-   *RIGHT*:		Create a neuron on the current hidden layer  
-   *SPACE*:		Start/Stop learning (Backpropagation)  
-   *MouseClick*:	Create an output to be learned when in Learning mode

**Important**: Please, unpack the Lib.zip for libs used in this project.

**Author**: Leonardo Correa Rocha  

leonardo.correa.rocha@hotmail.com  

